# deepClassification

deep learning classification for VIST

TO DO
- download corpora: civic, onckokb and pubmed (random sample)
- copy and update the code from statML code
- analyze the corpora for some statistics (see in lit which statistics)
- download pubmed w2v or ft (or train yours)


MODELS

1) multi-task for document classification
- pmid_text (title + abstract) -> [set of labels for each classificatin question: iscancer, whichcancer, clinically relevant] \
- input: title + abstract\
- w2v/ft trained on pubmed\
- multitask learning (http://ruder.io/multi-task/index.html#twomtlmethodsfordeeplearning)\
- different loss!

2) Hierarchical Attention Networks for Document Classification. \
single model for each classification question: iscancer, whichcancer, clinically relevant
- https://richliao.github.io/supervised/classification/2016/12/26/textclassifier-HATN/
- https://github.com/richliao/textClassifier/blob/master/textClassifierHATT.py

3) autoencoder for document representation, with encoder output the representation

https://machinelearningmastery.com/encoder-decoder-models-text-summarization-keras/
- we are interested in the initial decoder output and use that instead of current sentence "decoder"\
- train on PubMed (going to take a looong time)\
- train on a selected set of PubMed documents focused on cancer         