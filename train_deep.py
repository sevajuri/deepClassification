import numpy as np
np.random.seed(42)

from utils.models import Models
from statistics import median
import morph

from utils.transformers import TokenizePreprocessor
from utils.data_loader import data_loader
from keras.preprocessing.text import Tokenizer

dl = data_loader()
datasets = dl._trainData
tasks = dl._tasks

for dataset in datasets:

    tokenizer = TokenizePreprocessor()
    keras_tokenizer = Tokenizer()

    # READ DATASETS
    dataset_name = "_".join(dataset)
    df = dl.read_corpora(dataset, dataset_name, oversample_by_copy=True)

    # TRAING CORPORA - RAW -> PREPARED
    corpora = df.text.values
    keras_tokenizer, word_to_index_dict, index_to_word_dict, MAX_SENTS, MAX_SENT_LENGTH, tokens = dl.prepareForTraining(
        corpora, dataset_name, tokenizer, keras_tokenizer, median=True)

    print("Starting to work with {} dataset".format(dataset_name))

    for task in tasks:

        print("Starting to work with {} task".format(task))

        if task not in tasks.keys():
            raise ValueError('Task is one of {}'.format(self._tasks.keys()))

        model = Models()
        model_name = "{}_{}".format(task, dataset_name)
        # experiment = Experiment(api_key="S0J16MrSOvCzrrjQ9PmmqQYvp",
        #                         project_name=model_name,
        #                         workspace="sevajuri")

        if task == 'mtl':
            tokens = [morph.flatten(x) for x in tokens]
            MEDIAN_SEQUENCE_LENGTH = int(median([len(x) for x in tokens]))

            # PADDING
            data = np.zeros((len(tokens), MEDIAN_SEQUENCE_LENGTH), dtype='int32')
            for i, documents in enumerate(tokens):
                for k, token in enumerate(documents):
                    if k < MEDIAN_SEQUENCE_LENGTH:
                        data[i, k] = keras_tokenizer.word_index[token]

            embedding_matrix, embedding_layer = dl.get_embedding_layer(keras_tokenizer.word_index,
                                                                       MEDIAN_SEQUENCE_LENGTH, model_name)

            labels_is_cancer = dl.get_labels(df.loc[:, 'AboutCancer'], '{}_is_cancer'.format(model_name))
            labels_cancer_type = dl.get_labels(df.loc[:, 'doid'], '{}_cancer_type'.format(model_name))
            labels_clinical = dl.get_labels(df.loc[:, 'relevance'], '{}_clinical'.format(model_name))

            model.multi_task_model(data,
                                   labels_cancer_type,
                                   labels_is_cancer,
                                   labels_clinical,
                                   embedding_layer,
                                   model_name,
                                   dataset_name,
                                   # experiment.get_key(),
                                   batch_size=500,
                                   epochs=30
                           )

        else:
            data = np.zeros((len(tokens), MAX_SENTS, MAX_SENT_LENGTH), dtype='int32')
            # print(data.shape)
            for i, sentences in enumerate(tokens):
                for j, sent in enumerate(sentences):
                    if j < MAX_SENTS:
                        for k, token in enumerate(sent):
                            if k < MAX_SENT_LENGTH:
                                data[i, j, k] = keras_tokenizer.word_index[token]
            embedding_matrix, embedding_layer = dl.get_embedding_layer(keras_tokenizer.word_index, MAX_SENT_LENGTH, model_name)

            if task == 'mtl_hatt':
                labels_is_cancer = dl.get_labels(df.loc[:, 'AboutCancer'], '{}_is_cancer'.format(model_name))
                labels_cancer_type = dl.get_labels(df.loc[:, 'doid'], '{}_cancer_type'.format(model_name))
                labels_clinical = dl.get_labels(df.loc[:, 'relevance'], '{}_clinical'.format(model_name))
                model.multi_task_model_hatt(data,
                                       labels_cancer_type,
                                       labels_is_cancer,
                                       labels_clinical,
                                       embedding_layer,
                                       model_name,
                                       dataset_name,
                                       # experiment.get_key(),
                                       batch_size=500,
                                       epochs=30
                                            )

            else:
                labels = dl.get_labels(df.loc[:, tasks[task]['label_name']], model_name)
                hatt_model = model.hatt(
                    data,
                    labels,
                    embedding_layer,
                    model_name,
                    task,
                    dataset_name,
                    # experiment.get_key(),
                    batch_size=500,
                    epochs=30
                )