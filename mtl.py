import numpy as np
np.random.seed(42)

import pandas as pd
import morph
from statistics import median
from sklearn.utils import shuffle
from comet_ml import Experiment
from keras.preprocessing.text import Tokenizer
from utils.transformers import TokenizePreprocessor
from utils.data_loader import data_loader
from utils.models import Models

dl = data_loader()
tokenizer = TokenizePreprocessor()
keras_tokenizer = Tokenizer()

try:
    df = pd.read_csv('data/mtl_preprocessed_all.csv', delimiter='\t', dtype='str')
except FileNotFoundError:
    onco_negative = dl.load_dataset('onco_negative')
    onco_positive = dl.load_dataset('onco_positive')
    civic = dl.load_dataset('civic')
    pubmed = dl.load_dataset('pubmed')

    df = pd.concat([onco_negative, onco_positive, civic, pubmed])
    for index, row in df.loc[df.doid.isnull()].iterrows():
        doid = dl.getDOID(name=row.cancer)
    #    print('returned doid', doid, doid[row.cancer])
    #    input()
        if doid:
            if doid[row.cancer]=='162':
                df.loc[(df.cancer == row.cancer) & (df.doid.isnull()) , ['doid', 'cancer']] = '162', 'All Tumors'
            else:
                df.loc[(df.cancer == row.cancer) & (df.doid.isnull()) , ['doid']] = doid[row.cancer]
        else:
            df.loc[(df.cancer == row.cancer) & (df.doid.isnull()) , ['doid', 'cancer']] = '162', 'All Tumors'
            print('No DOID')

    # df = pd.concat([onco_negative, onco_positive, civic, pubmed]).dropna()
    # df.doid = df.doid.astype(int).astype(str)

    df = dl.getDoid_Parent(df)
    df.to_csv('data/mtl_preprocessed_all.csv', index=None,header=True, sep='\t')

df = dl.getDoid_Parent(df)
df = shuffle(df)

# PREPARE DATA: GET TRANSFORMED LABELS, TOKENIZED CORPORA, CREATE EMBEDDING LAYER AND MATRIX, CREATE TRAIN/TEST SPLIT
corpora = df.text.values
labels_cancer_type = dl.get_labels(df.iloc[:,[1]], 'mtl_cancer_type')
labels_is_cancer = dl.get_labels(df.iloc[:,[-1]], 'mtl_is_cancer')
labels_clinical = dl.get_labels(df.iloc[:,[-2]], 'mtl_clinical')

keras_tokenizer, word_to_index_dict, index_to_word_dict, max_sentence_nr, max_sentence_tokens_nr, tokens = dl.prepareForTraining(corpora, 'multi_task', tokenizer, keras_tokenizer)
tokens = [morph.flatten(x) for x in tokens]
MEDIAN_SEQUENCE_LENGTH = int(median([len(x) for x in tokens]))

# PADDING
data = np.zeros((len(tokens), MEDIAN_SEQUENCE_LENGTH), dtype='int32')
for i, documents in enumerate(tokens):
        for k, token in enumerate(documents):
            if k < MEDIAN_SEQUENCE_LENGTH:
                data[i, k] = keras_tokenizer.word_index[token]

embedding_matrix, embedding_layer = dl.get_embedding_layer(keras_tokenizer.word_index, MEDIAN_SEQUENCE_LENGTH)
print(data.shape, labels_cancer_type.shape, labels_is_cancer.shape, labels_clinical.shape)

experiment = Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj",
                        project_name="mtl_classification",
                        workspace="deakkon")

model = Models()
model.multi_task_model(data,
                       labels_cancer_type,
                       labels_is_cancer,
                       labels_clinical,
                       embedding_layer,
                       experiment.get_key(),
                       batch_size=500,
                       epochs=100
                   )