import pubmed_parser as pp
from random import randint
from time import sleep
import traceback

def getAbstract(inputData):
    pmid = inputData[-1]
    try:
        sleep(randint(0, 2))
        dict_out = pp.parse_xml_web(pmid, save_xml=False)
        # print(dict_out)
        if dict_out['title'] and dict_out['abstract']:
            # abstract = inputData + [dict_out['title'].strip() + '. ' + dict_out['abstract'].strip()]
            inputData.append(dict_out['title'].strip() + ' ' + dict_out['abstract'].strip())
        else:
            return None
    except Exception as e:
        print(e)
        return None

    return inputData

def getAbstracts(label, pmidList=None, url=None, downloadPath=None, index=None, delimiter='\t'):
    print(pmidList)

    # GET ABSTRACTS FOR PMIDS IN DATASET
    if not pmidList:
        print('pmids list empty.')
        pmidList = self.prepare_csv(url, downloadPath, index, delimiter)

    if pmidList is None:
        raise ValueError("No PMIDs to extract title and abstract for.")

    abstracts = []
    p = Pool(processes=cpu_count())
    skipped = 0
    try:
        # for res in tqdm(p.imap_unordered(self.getAbstract, pmidList), total=len(pmidList), ascii=True, desc='\tDownloading PubMed  abstracts'):
        for res in tqdm(p.imap_unordered(getAbstract, pmidList), total=len(pmidList)):
            if res:
                res.append(label)
                abstracts.append(res)
            else:
                skipped = + 1

    except KeyboardInterrupt:
        sys.exit(1)

    except Exception as e:
        traceback.print_exc()
        print('prepared abstracts')

    print('Skipped {} PMIDs'.format(skipped))
    return abstracts


def finilize(df, label):
    df['relevance']=label
    df['AboutCancer'] = 'Yes'
    return df


import pandas as pd
import os
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
import traceback
# from function_imports import getAbstract
import pubmed_parser as pp

data = '../data/original/relevance_User_ul.csv'
# cancer	doid	pmid	text	relevance	AboutCancer
original = pd.read_csv(data)
not_relevant = [['cancer', '162', x[1]] for x in original.loc[original.Relevance == 'irrelevant'].values.tolist() if
                '-' not in x[1]]
# print(not_relevant)

relevant = [['cancer', '162', x[1]] for x in original.loc[original.Relevance == 'relevant'].values.tolist() if
            '-' not in x[1]]

cols = ['cancer', 'doid', 'pmid', 'text', 'relevance']
relevant_text = pd.DataFrame(getAbstracts('Relevant', relevant), columns=cols)
relevant_text ['AboutCancer'] = 'Yes'
print(relevant_text.shape)

irrelevant_text = pd.DataFrame(getAbstracts('NotRelevant', not_relevant), columns=cols)
irrelevant_text ['AboutCancer'] = 'Yes'
print(irrelevant_text.shape)

merged = pd.concat([relevant_text, irrelevant_text])
print(merged)
print(len(merged.pmid.unique()))
merged.to_csv('bla.csv', sep='\t', index=False)
# for item in tqdm(relevant):
#     getAbstract(item)