#!/usr/bin/python
# -*- coding: utf-8 -*-
from nltk import TreebankWordTokenizer
from sklearn.base import BaseEstimator, TransformerMixin
import re
from unidecode import unidecode
# from _config import *
import string
import numpy as np
import pandas as pd
import tqdm
import pickle
# from fastText import load_model
import math
import datetime
from io import StringIO
import keras
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import Callback
from keras.layers import Embedding
from keras.utils import to_categorical

from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from collections import Counter
import random
import os, sys
import warnings
import traceback

if not sys.warnoptions:
    warnings.simplefilter("ignore")

#REPRODUCIBLE
np.random.seed(42)
random.seed(12345)
os.environ['PYTHONHASHSEED'] = '0'
now = datetime.datetime.now()
date_label=now.strftime("%Y_%m_%d")
lower_ahead = "(?=[a-z0-9])"
nonword_behind = "(?<=\W)"
# model_FR = load_model('data/embeddings/cc.fr.300.bin')
# model_HU = load_model('data/embeddings/cc.hu.300.bin')
# model_IT = load_model('data/embeddings/cc.it.300.bin')

regex_concept_dict = [
    # biomedical
    (re.compile("\w+inib$"), "_chemical_"),
    (re.compile("\w+[ui]mab$"), "_chemical_"),
    (re.compile("->|-->"), "_replacement_"),
    # (re.compile("^(PFS|pfs)$"), "progression-free survival"),

    # number-related concepts
    (re.compile("^[Pp]([=<>≤≥]|</?=|>/?=)\d"), "_p_val_"),
    (re.compile("^((\d+-)?year-old|y\.?o\.?)$"), "_age_"),
    (re.compile("^~?-?\d*[·.]?\d+--?\d*[·.]?\d+$"), "_range_"),
    (re.compile("[a-zA-Z]?(~?[=<>≤≥]|</?=|>/?=)\d?|^(lt|gt|geq|leq)$"), "_ineq_"),
    (re.compile("^~?\d+-fold$"), "_n_fold_"),
    (re.compile("^~?\d+/\d+$|^\d+:\d+$"), "_ratio_"),
    (re.compile("^~?-?\d*[·.]?\d*%$"), "_percent_"),
    (re.compile("^~?\d*(("
                "(kg|\d+g|mg|ug|ng)|"
                "(\d+m|cm|mm|um|nm)|"
                "(\d+l|ml|cl|ul|mol|mmol|nmol|mumol|mo))/?)+$"), "_unit_"),
    # abbreviation starting with letters and containing nums
    (re.compile("^[Rr][Ss]\d+$|"
                "^[Rr]\d+[A-Za-z]$"), "_mutation_"),
    (re.compile("^[a-zA-Z]\w*-?\w*\d+\w*$"), "_abbrev_"),
    # time
    (re.compile("^([jJ]an\.(uary)?|[fF]eb\.(ruary)?|[mM]ar\.(ch)?|"
                "[Aa]pr\.(il)?|[Mm]ay\.|[jJ]un\.(e)?|"
                "[jJ]ul\.(y)?|[aA]ug\.(ust)?|[sS]ep\.(tember)?|"
                "[oO]ct\.(ober)?|[nN]ov\.(ember)?|[dD]ec\.(ember)?)$"), "_month_"),
    (re.compile("^(19|20)\d\d$"), "_year_"),
    # numbers
    (re.compile("^(([Zz]ero(th)?|[Oo]ne|[Tt]wo|[Tt]hree|[Ff]our(th)?|"
                "[Ff]i(ve|fth)|[Ss]ix(th)?|[Ss]even(th)?|[Ee]ight(th)?|"
                "[Nn]in(e|th)|[Tt]en(th)?|[Ee]leven(th)?|"
                "[Tt]went(y|ieth)?|[Tt]hirt(y|ieth)?|[Ff]ort(y|ieth)?|[Ff]ift(y|ieth)?|"
                "[Ss]ixt(y|ieth)?|[Ss]event(y|ieth)?|[Ee]ight(y|ieth)?|[Nn]inet(y|ieth)?|"
                "[Mm]illion(th)?|[Bb]illion(th)?|"
                "[Tt]welv(e|th)|[Hh]undred(th)?|[Tt]housand(th)?|"
                "[Ff]irst|[Ss]econd|[Tt]hird|\d*1st|\d*2nd|\d*3rd|\d+-?th)-?)+$"), "_num_"),
    (re.compile("^~?-?\d+(,\d\d\d)*$"), "_num_"),  # int (+ or -)
    (re.compile("^~?-?((-?\d*[·.]\d+$|^-?\d+[·.]\d*)(\+/-)?)+$"), "_num_"),  # float (+ or -)
    # misc. abbrevs
    (re.compile("^[Vv]\.?[Ss]\.?$|^[Vv]ersus$"), "vs"),
    (re.compile("^[Ii]\.?[Ee]\.?$"), "ie"),
    (re.compile("^[Ee]\.?[Gg]\.?$"), "eg"),
    (re.compile("^[Ii]\.?[Vv]\.?$"), "iv"),
    (re.compile("^[Pp]\.?[Oo]\.?$"), "po")
]

prenormalize_dict = [
    # common abbreviations
    (re.compile(nonword_behind + "[Cc]a\.\s" + lower_ahead), "ca  "),
    (re.compile(nonword_behind + "[Ee]\.[Gg]\.\s" + lower_ahead), "e g  "),
    (re.compile(nonword_behind + "[Ee][Gg]\.\s" + lower_ahead), "e g "),
    (re.compile(nonword_behind + "[Ii]\.[Ee]\.\s" + lower_ahead), "i e  "),
    (re.compile(nonword_behind + "[Ii][Ee]\.\s" + lower_ahead), "i e "),
    (re.compile(nonword_behind + "[Aa]pprox\.\s" + lower_ahead), "approx  "),
    (re.compile(nonword_behind + "[Nn]o\.\s" + lower_ahead), "no  "),
    (re.compile(nonword_behind + "[Nn]o\.\s" + "(?=\w\d)"), "no  "),  # no. followed by abbreviation (patient no. V123)
    (re.compile(nonword_behind + "[Cc]onf\.\s" + lower_ahead), "conf  "),
    # scientific writing
    (re.compile(nonword_behind + "et al\.\s" + lower_ahead), "et al  "),
    (re.compile(nonword_behind + "[Rr]ef\.\s" + lower_ahead), "ref  "),
    (re.compile(nonword_behind + "[Ff]ig\.\s" + lower_ahead), "fig  "),
    # medical
    (re.compile(nonword_behind + "y\.o\.\s" + lower_ahead), "y o  "),
    (re.compile(nonword_behind + "yo\.\s" + lower_ahead), "y o "),
    (re.compile(nonword_behind + "[Pp]\.o\.\s" + lower_ahead), "p o  "),
    (re.compile(nonword_behind + "[Ii]\.v\.\s" + lower_ahead), "i v  "),
    (re.compile(nonword_behind + "[Bb]\.i\.\d\.\s" + lower_ahead), "b i d  "),
    (re.compile(nonword_behind + "[Tt]\.i\.\d\.\s" + lower_ahead), "t i d  "),
    (re.compile(nonword_behind + "[Qq]\.i\.\d\.\s" + lower_ahead), "q i d  "),
    (re.compile(nonword_behind + "J\.\s" + "(?=(Cell|Bio|Med))"), "J  "),  # journal
    # bracket complications
    # (re.compile("\.\)\."), " )."),
    # (re.compile("\.\s\)\."), "  )."),
    # multiple dots
    # (re.compile("(\.+\s*\.+)+"), "."),
    # # Typos: missing space after dot; only add space if there are at least two letters before and behind
    # (re.compile("(?<=[A-Za-z]{2})" + "\." + "(?=[A-Z][a-z])"), ". "),
    # whitespace
    (re.compile("\s"), " "),
]

def report_to_df(report):
    report = re.sub(r" +", " ", report).replace("avg / total", "avg/total").replace("\n ", "\n")
    report_df = pd.read_csv(StringIO("Classes" + report), sep=' ', index_col=0)
    return(report_df)

def map_regex_concepts(token):
    """replaces abbreviations matching simple REs, e.g. for numbers, percentages, gene names, by class tokens"""
    for regex, repl in regex_concept_dict:
        if regex.findall(token):
            return repl
    return token

def prenormalize(text):
    """normalize common abbreviations and symbols known to mess with sentence boundary disambiguation"""
    for regex, repl in prenormalize_dict:
        text = regex.sub(repl, text)
    return text

def flatten(l):
    """flatten 2-dimensional sequence to one-dimensional"""
    return [item for sublist in l for item in sublist]

class TextNormalizer(BaseEstimator, TransformerMixin):
    """replaces all non-ASCII characters by approximations, all numbers by 1"""

    def __init__(self):
        return

    def fit(self, X=None, y=None):
        return self

    @staticmethod
    def transform(X):
        return np.array([unidecode(x) for x in X])

class TokenizePreprocessor(BaseEstimator, TransformerMixin):
    def __init__(self, rules=True):
        self.punct = set(string.punctuation).difference(set('%='))
        self.rules = rules
        self.splitters = re.compile("[-/.,|<>]")
        self.tokenizer = TreebankWordTokenizer()

    def fit(self, X=None, y=None):
        return self

    @staticmethod
    def inverse_transform(X):
        return [", ".join(doc) for doc in X]

    def transform(self, X):
        return [self.token_representation(sentence) for sentence in X]

    def token_representation(self, sentence):
        return list(self.tokenize(sentence))

    def tokenize(self, sentence):
        """break sentence into pos-tagged tokens; normalize and split on hyphens"""

        # extremely short sentences shall be ignored by next steps

        # if len(sentence) < MIN_LEN:
        #     yield "_empty_sentence_"
        # else:

        for token in self.tokenizer.tokenize(sentence):
            # Apply preprocessing to the token
            token_nrm = self.normalize_token(token)
            subtokens = [self.normalize_token(t) for t in self.splitters.split(token_nrm)]

            for subtoken in subtokens:
                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue
                yield subtoken

    def normalize_token(self, token):
        # Apply preprocessing to the token
        token = token.lower().strip().strip('*').strip('.')
        if self.rules:
            token = map_regex_concepts(token)
        return token

class KerasBatchGenerator(keras.utils.Sequence):

    def __init__(self, batch_size, source_corpus, source_maxlen, source_tokenizer, target_corpus, target_maxlen, target_tokenizer):

        self.batch_size = batch_size

        self.source_corpus = source_corpus
        self.target_corpus = target_corpus

        self.source_maxlen = source_maxlen
        self.target_maxlen = target_maxlen

        self.source_tokenizer = source_tokenizer
        self.target_tokenizer = target_tokenizer
        self.current_idx = 0


    def generate_data(self):
        while True:
            if self.current_idx * self.batch_size >= len(self.source_corpus):
                # reset the index back to the start of the data set
                self.current_idx = 0

            batch_source = self.source_corpus[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_source = self.source_tokenizer.texts_to_sequences(batch_source)
            batch_source = pad_sequences(batch_source, maxlen=self.source_maxlen, padding='post')

            batch_target = self.target_corpus[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_target = self.target_tokenizer.texts_to_sequences(batch_target)

            # target_train_onehot = np.zeros((self.batch_size, self.target_maxlen, len(self.target_tokenizer.word_index) + 1))
            target_train_onehot = np.zeros((len(batch_source), self.target_maxlen, len(self.target_tokenizer.word_index) + 1))
            for seq_id, sequence in enumerate(batch_target):
                for item_id, item in enumerate(sequence):
                    if item_id > 0:
                        target_train_onehot[seq_id][item_id - 1][int(item)] = 1

            batch_target = pad_sequences(batch_target, maxlen=self.target_maxlen, padding='post')
            self.current_idx += 1
            yield [batch_source, batch_target], target_train_onehot

class KerasBatchGenerator_VAE(keras.utils.Sequence):

    def __init__(self, batch_size, source_corpus, source_maxlen, vocabulary_size, shuffle=True):
        self.batch_size = batch_size
        self.source_corpus = source_corpus
        self.sequence_length = source_maxlen
        self.vocabulary_size = vocabulary_size
        self.shuffle = shuffle
        self.current_idx = 0
        self.on_epoch_end()
        
    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.source_corpus) / self.batch_size))

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        # self.indexes = np.arange(len(self.source_corpus))
        if self.shuffle == True:
            np.random.shuffle(self.source_corpus)

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        # indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        #
        # # Find list of IDs
        # list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation()

        return X, y

    def __data_generation(self):
        while True:
            if self.current_idx * self.batch_size >= len(self.source_corpus):
                # reset the index back to the start of the data set
                self.current_idx = 0

            batch_source = self.source_corpus[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            # input(batch_source.shape)
            # input(batch_source)
            # input(type(batch_source))

            # target_train_onehot = np.zeros((self.batch_size, self.target_maxlen, len(self.target_tokenizer.word_index) + 1))
            temp = np.zeros((batch_source.shape[0], self.sequence_length, self.vocabulary_size))
            temp[np.expand_dims(np.arange(batch_source.shape[0]), axis=0).reshape(batch_source.shape[0], 1),
                 np.repeat(np.array([np.arange(self.sequence_length)]), batch_source.shape[0], axis=0), batch_source] = 1
            X_train_one_hot = temp
            # X_train_one_hot = to_categorical(batch_source)

            self.current_idx += 1
            return batch_source, X_train_one_hot

class KerasBatchGeneratorClassification(keras.utils.Sequence):

    def __init__(self, batch_size, data, labels, timesteps, tokenizer, labeler):

        self.batch_size = batch_size
        self.data = data
        self.labels = labels
        self.timesteps = timesteps
        self.tokenizer = tokenizer
        self.labeler = labeler
        # this will track the progress of the batches sequentially through the
        # data set - once the data reaches the end of the data set it will reset
        # back to zero
        self.current_idx = 0


    def generate_data(self):
        while True:
            if self.current_idx * self.batch_size >= len(self.source_corpus):
                # reset the index back to the start of the data set
                self.current_idx = 0

            batch_source = self.data[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_source = self.tokenizer.texts_to_sequences(batch_source)
            batch_source = pad_sequences(batch_source, maxlen=self.timesteps, padding='post')

            batch_labels = self.labels[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_labels = self.labeler.transform(batch_labels)

            self.current_idx += 1
            yield batch_source, batch_labels

class Metrics(Callback):

    def on_train_begin(self, logs={}):
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []


    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.model.validation_data[0]))).round()
        val_targ = self.model.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict)
        _val_recall = recall_score(val_targ, val_predict)
        _val_precision = precision_score(val_targ, val_predict)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print(" — val_f1: % f — val_precision: % f — val_recall % f" % (_val_f1, _val_precision, _val_recall))
        return

class prepareData():

    def __init__(self):
        # self.tokenizer = TokenizePreprocessor()
        # self.vocabulary=None
        pass

    def prepareData(self, dataset, multilabel=True):
        data = None
        errors = None

        if multilabel:
            fileName = 'data/preprocesed/Train_multilabel_{}.p'.format(dataset)
        else:
            fileName = 'data/preprocesed/Train_{}.p'.format(dataset)

        try:
            data = pickle.load(open(fileName,'rb'))
            # errors = pickle.load(open(fileName,'rb'))

        except FileNotFoundError:
            file_encoding = TRAINING[dataset]['Encoding']
            split_multi_code = TRAINING[dataset]['SplitMultiCode']

            cc_files = TRAINING[dataset]['CC']
            cc_data = [pd.read_csv(cc_file, sep=';', encoding=file_encoding, skipinitialspace=True) for cc_file in cc_files]
            cc = pd.concat(cc_data)

            cb_files = TRAINING[dataset]['CB']
            cb_data = [pd.read_csv(cb_file, sep=';', encoding=file_encoding, skipinitialspace=True) for cb_file in cb_files]
            cb = pd.concat(cb_data)

            data = []
            errors= []

            for index, row in tqdm.tqdm(cb.iterrows(), ascii=True, desc='Preparing {}'.format(dataset)):
            # for index, row in cb.iterrows():
            # for index, row in tqdm.tqdm(cb.groupby(['DocID','YearCoded','LineID']), ascii=True, desc='Preparing {}'.format(dataset)):

                try:
                    # print(row.DocID, row.LineID, row.YearCoded)
                    text = cc[(cc.DocID == row.DocID) & (cc.LineID == row.LineID) & (cc.YearCoded == row.YearCoded)]

                    if len(text) == 0:
                        # print('empty')
                        text = cc[(cc.DocID == row.DocID) & (cc.LineID == row.LineID+1) & (cc.YearCoded == row.YearCoded)]

                    # text = text.dropna()
                    num_icd10_codes = len(text)

                    appended_data = False
                    if split_multi_code and num_icd10_codes > 1:
                        parts = row.RawText.lower().split(",")
                        if len(parts) == num_icd10_codes:
                            for i in range(num_icd10_codes):
                                data.append([parts[i], text.StandardText.values[i], text.ICD10.values[i]])

                            appended_data = True

                    if not appended_data and text.StandardText.values.tolist() and text.ICD10.values.tolist():
                        if multilabel:
                            try:
                                data.append([
                                    row.RawText.lower(),
                                    [x.lower() for x in text.StandardText.dropna().values],
                                    [x.lower() for x in text.ICD10.dropna().values]
                                ])
                            except Exception as exe:
                                pass
                                # print(traceback.print_exc())
                                # print(                row.RawText.lower(),
                                # [x.lower() for x in text.StandardText.values],
                                # [x.lower() for x in text.ICD10.values])
                                # input('joj')

                        else:
                            data.append([
                                row.RawText.lower(),
                                text.StandardText.values[0].lower(),
                                text.ICD10.values[0].lower()
                            ])

                except Exception as e:
                    print(traceback.print_exc())
                    #print('error on ', row.DocID, row.LineID)
                    errors.append([dataset, row.DocID, row.LineID])
                    # input()

            output_folder = "data/preprocesed/"
            os.makedirs(output_folder, exist_ok=True)

            pickle.dump(data, open(fileName,'wb'))
            # pickle.dump(errors, open(fileName,'wb'))

        # random.shuffle(data)
        return data, errors

    def prepareDictionaries(self, unbalanced=False, oversampled=False):

        preparedDictionary = []
        dicts = [
            DICT_FR,
            DICT_HU,
            DICT_IT
        ]

        for item in dicts:
            df = pd.read_csv(item, sep=';', dtype=str, encoding = "utf8")
            for index, row in df.iterrows():
                try:
                    text = ' '.join([row['DiagnosisText'], row['Standardized']])
                except (KeyError, TypeError):
                    text = row['DiagnosisText']

                label = str(row['Icd1']).upper()[:4].strip()

                if not isinstance(text, float):
                    preparedDictionary.append([text.lower(), label])
                else:
                    if not math.isnan(text):
                        preparedDictionary.append([ text.lower(), label ])

        if unbalanced:
            for k, v in TRAINING.items():
                df = pd.read_csv(v['CC'][0], sep=';', dtype=str, encoding="utf8")
                for index, row in df.iterrows():
                    label = str(row['ICD10']).strip().upper()[:4]
                    text = row['StandardText']

                    if not isinstance(text, float):
                        preparedDictionary.append([text.lower().strip(), label])
                    else:
                        if not math.isnan(text):
                            preparedDictionary.append([text.lower().strip(), label])

        if oversampled:
            tmp = [x[1] for x in preparedDictionary]
            labels_c = Counter(tmp)
            labels_tmp = {k: v for k, v in labels_c.items() if v < 3}
            undersampled = [x for x in preparedDictionary if x[1] in labels_tmp] * 3
            preparedDictionary += undersampled

            # pickle.dump(preparedDictionary, open(DICT_PREPROCESED, 'wb'))
        random.shuffle(preparedDictionary)
        corpus = [item[0] for item in preparedDictionary]
        labels = [item[1] for item in preparedDictionary]
        #print(Counterlabels)
        return corpus, labels

    def prepareForTraining(self, corpora, fileName, tokenizer=None, kerasTokenizer=None):
        # PREPARE CORPORA FOR S2S MODEL
        # corpora -> list of lists
        # fileName -> source or target
        # tokenizer -> split sentence to tokens
        # kerasTokenizer -> tokenizer for keras models and mappings
        # return: tokenizer, word->id mapping, id->word mapping, vocabulary

        validFileName = {'source', 'target'}
        if fileName not in validFileName:
            raise ValueError("results: fileName must be one of %r." % validFileName)

        if any(isinstance(el, list) for el in corpora):
            print('multilabel')
            corpora = [['sos ' + ' tab '.join(x) + ' eos'] for x in corpora]
            fileName='models/s2s_{}_tokenizer_multilabel_extended.p'.format(fileName)
            corpora = flatten(corpora)
            # multilabel
        else:
            print('se bon se merde')
            corpora = [['sos ' + x + ' eos'] for x in corpora]
            fileName='models/s2s_{}_tokenizer_extended.p'.format(fileName)

        try:
            with open(fileName, 'rb') as f:
                pData = pickle.load(f)

            kerasTokenizer = pData['tokenizer']
            vocab = pData['w2i']
            index_to_word_dict = pData['i2w']
            max_sequence_tokenizer = pData['seq_length']
        except OSError:
            tokens = tokenizer.transform([x for x in corpora])
            print(tokens)
            input('tokens')
            max_sequence_tokenizer = max([len(x) for x in tokens])
            tmp = [item for item in list(set(flatten(tokens))) if item.strip()]
            vocab = {item.strip(): i + 1 for i, item in enumerate(tmp)}
            index_to_word_dict = {i + 1: item.strip() for i, item in enumerate(tmp)}
            kerasTokenizer.word_index = vocab
            with open(fileName, 'wb') as handle:
                pickle.dump({'tokenizer': kerasTokenizer,
                             'w2i': vocab,
                             'i2w': index_to_word_dict,
                             'seq_length': max_sequence_tokenizer
                             }, handle)

        return kerasTokenizer, vocab, index_to_word_dict, max_sequence_tokenizer

    def get_embedding_layer(self, vocabulary, max_seq_length, name, EMBEDDING_DIM=900):

        validFileName = {'source', 'target'}
        if name not in validFileName:
            raise ValueError("results: name must be one of %r." % validFileName)

        embedding_matrix = np.zeros((len(vocabulary) + 1, EMBEDDING_DIM))
        # print(len(vocabulary), embedding_matrix.shape)
        for word, i in vocabulary.items():
            embedding_vector_FR = model_FR.get_word_vector(word)
            embedding_vector_HU = model_HU.get_word_vector(word)
            embedding_vector_IT = model_IT.get_word_vector(word)
            embedding_matrix[i] = np.concatenate([embedding_vector_FR, embedding_vector_HU, embedding_vector_IT])
        # return embedding_matrix

        return Embedding(embedding_matrix.shape[0],
                         embedding_matrix.shape[1],
                         weights=[embedding_matrix],
                         input_length=max_seq_length,
                         trainable=True,
                         mask_zero=True,
                         name='{}_embedding'.format(name))

    # def createSplits(self, corpora, labels):
    #
    #     # generate train/test split
    #     if any(isinstance(el, list) for el in labels):
    #
    #
    #
    #     else:
    #         source_train, source_val, _, _ = train_test_split(source_corpus, labels, test_size=0.1,
    #                                                           random_state=777)
    #     target_train, target_val, labels_train, labels_val = train_test_split(target_corpus, labels, test_size=0.1,
    #                                                                           random_state=777)
    #
    #     data_set_train_test = {
    #         'source_train': source_train,
    #         'source_val': source_val,
    #         'target_train': target_train,
    #         'target_val': target_val,
    #         'labels_train': labels_train,
    #         'labels_val': labels_val
    #     }
    #
    #     with open('models/train_test_split_extended.p', 'wb') as handle:
    #         pickle.dump(data_set_train_test, handle)