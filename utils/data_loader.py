import numpy as np
np.random.seed(42)

import pandas as pd
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
import pickle
import traceback
import pubmed_parser as pp
import morph
from collections import Counter
from statistics import median

from random import randint, shuffle
from time import sleep
from statistics import median as median_stat
import pdb, traceback, sys

from gensim.models import Word2Vec, KeyedVectors
from gensim.test.utils import datapath
import fastText

from utils.utils import flatten
from utils.transformers import sentence_tokenize, TokenizePreprocessor

from keras.preprocessing.text import Tokenizer
from keras.layers import Embedding
from keras.utils.np_utils import to_categorical

from sklearn.preprocessing import LabelBinarizer, LabelEncoder, MultiLabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from sklearn.utils import shuffle as sk_shuffle

import requests
from bs4 import BeautifulSoup

np.random.seed(42)

class data_loader():

    def __init__(self, use_fastText = True):

        # fastText == True -> use fastText pretrained otherwise use word2vec

        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)     Chrome/37.0.2049.0 Safari/537.36'
        }

        self._storeData = 'data/prepared/'
        self._originalData = 'data/original/'
        self._queryData = 'data/queries/'
        self._trainFolder = 'data/train_data/'
        self._storeUtilities = 'trained_models/utilityModels/'

        #EMBEDDINGS
        self.use_fastText = use_fastText
        self.embedding_model = None

        self._datasetLabels ={
            'onco_positive':{
                'label':'Relevant',
                'aboutCancer':'Yes',
                'url': 'http://oncokb.org/api/v1/utils/allActionableVariants.txt',
                'columns': [6,-2]
                },
            'onco_negative':{
                'label':'NotRelevant',
                'aboutCancer': 'Yes',
                'url': 'http://oncokb.org/api/v1/utils/allAnnotatedVariants.txt',
                'columns': [-2]
            },
            'civic':{
                'label':'Relevant',
                'aboutCancer': 'Yes',
                'url':'http://civic.genome.wustl.edu/downloads/nightly/nightly-ClinicalEvidenceSummaries.tsv',
                'columns':[3,4,12]
            },
            'pubmed':{
                'label': 'NotRelevant',
                'aboutCancer': 'No',
                'url': None,
                'columns': [3, 4, 12]
            },
            'docm':{
                'url':'http://www.docm.info/api/v1/variants.tsv?undefined=&version=Current+(3.2)&mutation_types=&genes=&publications=&batches=&diseases=&tags=&amino_acids=&chromosomes=&position_start=&position_stop=',
                'label': 'Relevant',
                'aboutCancer': 'Yes',
                'columns': [-2, -1]
            },
            'cgi_biomarkers':{
                # 'url': 'http://www.docm.info/api/v1/variants.tsv?undefined=&version=Current+(3.2)&mutation_types=&genes=&publications=&batches=&diseases=&tags=&amino_acids=&chromosomes=&position_start=&position_stop=',
                'url':self._originalData+'cgi_biomarkers.tsv',
                'label': 'Relevant',
                'aboutCancer': 'Yes',
                # 'columns':['Primary Tumor type full name','Source']
                'columns': [-1, 11]
            },
            'evaluation_tumorboard':{
                'url': self._originalData + 'mario_neu.txt',
                'label': 'Relevant',
                'aboutCancer': 'Yes',
                # 'columns':['Primary Tumor type full name','Source']
                'columns': [-2]
            }
        }

        self._trainData = [
            # ['civic', 'pubmed'],
            ['docm', 'pubmed'],
            ['cgi_biomarkers', 'pubmed'],
            ['cgi_biomarkers', 'civic', 'docm', 'pubmed'],
            ['cgi_biomarkers', 'civic', 'pubmed']
        ]

        self._tasks = {
            'mtl_hatt': {
                'data': ['civic', 'onco_negative', 'onco_positive'],
                'label_index': [-1, 1, -2],
                'label_name': ['AboutCancer', 'doid', 'relevance'],
                'evaluate': ['gs_eval', ' tumorboard']
            },
            'is_cancer':{
                'data':['civic', 'pubmed'],
                'label_index':[-1],
                'label_name': 'AboutCancer',
                'evaluate':['onco_positive', 'gs_eval', 'tumorboard_dataset.p']
            },
            'cancer_type':{
                'data':['civic'],
                'label_index':[1],
                'label_name': 'doid',
                'evaluate':['onco_positive'],
            },
            'clinical':{
                'data':['civic', 'onco_negative','onco_positive'],
                'label_index':[-2],
                'label_name': 'relevance',
                'evaluate':['gs_eval',' tumorboard']
            },
            'mtl':{
                'data': ['civic', 'onco_negative', 'onco_positive'],
                'label_index': [-1,1,-2],
                'label_name': ['AboutCancer', 'doid', 'relevance'],
                'evaluate': ['gs_eval', ' tumorboard']
                }
            }

        self.prepared_datasets = {
            'onco_balanced':None,
            'onco_full': None,
            'civic_onco':None,
            'civic':None
        }

        self._cancer_doid_stop = {
            'gastrointestinal system cancer': '3119',
            'melanoma': '1909',
            'sarcoma': '1115',
            'malignant mesothelioma': '1790',
            'gangliocytoma': '2426',
            'musculoskeletal system cancer': '0060100',
            'mesenchymal cell neoplasm': '3350',
            'mixed cell type cancer': '154',
            'malignant hemangioma': '0080189',
            'endocrine gland cancer': '170',
            'head and neck cancer': '11934',
            'germ cell cancer': '2994',
            'blastoma': '0070003',
            'cell type cancer': '0050687',
            'peritoneum cancer': '1725',
            'carcinoma': '305',
            'malignant glioma': '3070',
            'cardiovascular cancer': '176',
            'nervous system cancer': '3093',
            'respiratory system cancer': '0050615',
            'organ system cancer': '0050686',
            'urinary system cancer': '3996',
            'NA': '-1',
            'reproductive organ cancer': '193',
            'immune system cancer': '0060083',
            'integumentary system cancer': '0060122',
            'thoracic cancer': '5093',
            'cancer':'162'
            }

        self._doid_cancer_stop = {v:k for k,v in self._cancer_doid_stop.items()}
        self.tokenizer = TokenizePreprocessor()


    def load_embedding_model(self):
        #EMBEDDINGS
        # self.use_fastText = use_fastText

        self._embeddingsStore = '/home/shared/embeddings/'

        print("Loading embedding models")
        if self.use_fastText:
            self.embedding_model = fastText.load_model(self._embeddingsStore+'BioWordVec_PubMed_MIMICIII_d200.bin')
        else:
            self.embedding_model = KeyedVectors.load_word2vec_format(datapath(self._embeddingsStore + "wikipedia-pubmed-and-PMC-w2v.bin"), binary=True)
        print("Done loading embedding models")

    def oversample_duplicate(self, dataset, min_support=5):

        frames = [dataset]
        counts = {k:v for k,v in Counter(dataset.doid.values.tolist()).items() if v < min_support}

        for k,v in counts.items():
            tmp_df = dataset.loc[dataset.doid == k]
            tmp_df=pd.concat([tmp_df] * 5, ignore_index=True)
            frames.append(tmp_df)

        return pd.concat(frames, ignore_index=True)

    def getDoid_Parent(self, dataset):

        # get parent DOID for all with support < 3; trying to group as much as possible
        # dataframe -> update all rows with doid_

        def get_info(self , doid):

            name = None
            try:
                r = requests.get('http://www.disease-ontology.org/api/metadata/DOID:{}'.format(doid))
                # print(r.url)
                # print(r.json()['parents'])
                tmp = r.json()
                name = tmp['parents'][0][1]
                doid = tmp['parents'][0][2][5:]
                # print(name, doid)

            except Exception:
                # WE ARE AT THE ROOT NOTE (DISEASE)
                # traceback.print_exc()
                pass

            return name, doid

        all_counts = Counter(dataset.doid.values.tolist())
        counts = {k:v for k,v in Counter(dataset.doid.values.tolist()).items() if v < 5}
        # input('{}\n\n{}\n *****'.format(all_counts, counts))

        for k,v in counts.items():
            found = False
            doid = k
            count = v
            name = None

            try:
                while not found:

                    name, doid = get_info(self, doid)
                    # print(k, type(k), doid, type(doid), all_counts[doid], 'Found name {} and doid {}'.format(name, doid) )

                    if name and doid:
                        try:
                            all_counts[doid]+=1
                            if all_counts[doid] > 2:
                                dataset.loc[dataset.doid == k, ['doid', 'cancer']] = doid, name
                                found = True
                                # input('Found root')
                        except KeyError:
                            # input('not in counter: {}'.format(doid))
                            all_counts[doid] = 1
                    else:
                        dataset.loc[dataset.doid == k, ['doid', 'cancer']] = '-1', 'No Cancer'
                        # input('No parents for {}: {}'.format(k,v))
                        found = True

            except Exception:
                traceback.print_exc()
                print('something is wrong')
                break

        return dataset

    def getAbstract(self, inputData):

        pmid = inputData[-1]
        # POSSIBLE WORLDS

        try:
            sleep(randint(0, 2))
            dict_out = pp.parse_xml_web(pmid, save_xml=False)
            # print(dict_out)
            if dict_out['title'] and dict_out['abstract']:
                # abstract = inputData + [dict_out['title'].strip() + '. ' + dict_out['abstract'].strip()]
                inputData.append(dict_out['title'].strip() + ' ' + dict_out['abstract'].strip())
            else:
                return None
        except Exception:
            # traceback.print_exc()
            return None

        return inputData

    def getAbstracts(self, label, pmidList=None, url=None, downloadPath=None, index=None, delimiter='\t'):

        # GET ABSTRACTS FOR PMIDS IN DATASET
        if not pmidList:
            print('pmids list empty.')
            pmidList = self.prepare_csv(url, downloadPath, index, delimiter)

        if pmidList is None:
            raise ValueError("No PMIDs to extract title and abstract for.")

        # pmidList = [tuple(x) for x in pmidList]
        # print('pmidList\t', pmidList)
        # input('getting ready to extract abstracts')

        # GET ABSTRACTS FOR PMIDS
        # pmidList: list of pmids to query for abstracts and titles
        # label: ['Relevant','Notrelevant']
        abstracts = []
        p = Pool(processes=cpu_count())
        skipped = 0
        try:
            # for res in tqdm(p.imap_unordered(self.getAbstract, pmidList), total=len(pmidList), ascii=True, desc='\tDownloading PubMed  abstracts'):
            for res in tqdm(p.imap_unordered(self.getAbstract, pmidList), total=len(pmidList)):
                if res:
                    res.append(label)
                    abstracts.append(res)
                else:
                    skipped =+ 1

        except KeyboardInterrupt:
            sys.exit(1)

        except Exception as e:
            traceback.print_exc()
            print('prepared abstracts')

        # for abst in abstracts:
        #     print(abst)
        #     input('abst')
        print('Skipped {} PMIDs'.format(skipped))
        return abstracts

    def normalize_to_doid(self, lookup_name, lookup_doid, original_name):
        # NORMALIZE CANCER NAME AND DOID TO A LIMITED LIST OF CANCER NAES
        # we only have a name and no doid -> need doid to traverse the tree to root nodes or non_cancer conclusion
        # input(self._doid_cancer_stop)
        if not lookup_doid:
            try:
                r = requests.get('http://disease-ontology.org/search?q={}'.format(lookup_name))
                # print(r.url)
                soup = BeautifulSoup(r.text, 'html.parser')
                lookup_doid = soup.findAll("td", {"class": "tbl-doid"})[0].text.lower().strip()[5:]
                # print(lookup_doid)
                if lookup_doid in self._doid_cancer_stop:
                    # print("Returning from all eve")
                    return (self._doid_cancer_stop[lookup_doid], original_name)
            except IndexError:
                # traceback.print_exc()
                tmp_name = lookup_name.split()[1:]
                # print('tmp_name\t', tmp_name)
                if tmp_name:
                    lookup_name = ' '.join(tmp_name).lower()
                    return self.normalize_to_doid(lookup_name, None, original_name)
                else:
                    # print('Not found after splitting. Returning NA')
                    return ('NA', original_name)

            except Exception:
                traceback.print_exc()
                print('http://disease-ontology.org/search?q={}'.format(lookup_name))

        # doid not in final list: get parent and move on with life
        r = requests.get('http://www.disease-ontology.org/api/metadata/DOID:{}'.format(lookup_doid))
        # print(r.url)
        tmp = r.json()
        # print(tmp)
        try:
            lookup_name = tmp['parents'][0][1].strip().lower()
            lookup_doid = tmp['parents'][0][2][5:]
        except KeyError:
            return ('NA', original_name)

        if lookup_doid in self._doid_cancer_stop:
            # got the doid we are normalizing for (i.e. looking for a parent)
            # print('FOUND:\t', lookup_name, lookup_doid, original_name)
            return (lookup_name, original_name)
        else:
            # print(lookup_doid, type(lookup_doid))
            return self.normalize_to_doid(lookup_name, lookup_doid, original_name)

    def prepare_csv(self, url, delimiter='\t'):
        # DOWNLOAD FILE FROM URL AND EXTRACT PMIDs
        items = None

        if url not in self._datasetLabels.keys():
            raise ValueError("url must be one of %r." % self._datasetLabels.keys())

        if url == 'pubmed':
            raise ValueError("PubMed dataset currently not supported")

        try:
            print("Preparing {} dataset".format(url))
            tmp_df = pd.read_csv(self._datasetLabels[url]['url'],
                         delimiter=delimiter,
                         encoding="ISO-8859-1",
                         dtype='str')
            df=tmp_df.iloc[:,self._datasetLabels[url]['columns']]
            df.drop_duplicates()

            if url == 'cgi_biomarkers':
                df['Primary Tumor type full name'] = df['Primary Tumor type full name'].str.replace(';', ',')
                df.Source = df.Source.str.replace(';', ',')

            if url not in ['cgi_biomarkers','evaluation_tumorboard']:
                tmp_df.to_csv(self._originalData + url + '_original.csv', index=None, header=True, sep='\t')

        except Exception:
            traceback.print_exc()

        # ADD DUMMMY DOID VALUE FOR DATASETS WITH 2 COLUMNS (SANS DOID VALUES)
        items = df.values.tolist()
        if df.shape[1] == 1:
            print("\tAdding dummy Cancer Name to values. Datasets have no cancer information and are therefore not used for cancer_type classification, but for clinical and cancer relevance.")
            if self._datasetLabels[url]['aboutCancer'] == 'Yes':
                [item.insert(0, 'cancer') for item in items]
            if self._datasetLabels[url]['aboutCancer'] == 'No':
                [item.insert(0, 'NA') for item in items]

        # SEPERATE DISEASE NAMES IF MULTIPLE
        processed_items = []
        for item in tqdm(items, ascii=True, desc="\tSeperating multiple disease names."):
            # print(item)
            try:
                diseases = [x.strip().lower() for x in item[0].split(',') if x.strip().lower()]
                for disease in diseases:
                    tmp_item = [disease] + item[1:]
                    processed_items.append(tmp_item)
            except AttributeError:
                print(item)
        dummy_df = pd.DataFrame(processed_items)
        dummy_df=dummy_df.drop_duplicates()
        items = dummy_df.values.tolist()

        # SPLIT LIST OF PMIDS TO INDIVIDUAL PMIDS
        processed_items = []
        # print("\tSplitting PMID lists")
        for item in tqdm(items, ascii=True, desc="\tSplitting PMID lists."):
            pmids = [str(x).strip() for x in str(item[-1]).split(',') if str(x).strip()]
            for pmid in pmids:
                tmp = item[:-1] + [pmid]
                processed_items.append(tmp)
        dummy_df = pd.DataFrame(processed_items)
        dummy_df =dummy_df.drop_duplicates()
        items = dummy_df.values.tolist()

        # ADD DUMMY DOID COLUMN TO DATASETS WITH NO DOID
        if dummy_df.shape[1] == 2:
            print("\tAdding dummy DOID values.")
            [item.insert(1, '-1') for item in items]

        # SPECIAL RULES FOR CGI_BIOMARKERS DATASET
        dummy_df = pd.DataFrame(items, columns=['cancer', 'doid', 'pmid'])
        if url == 'cgi_biomarkers':
            dummy_df = dummy_df[dummy_df.pmid.str.contains('PMID')]
            dummy_df.pmid = dummy_df.pmid.str.slice(5, )
        dummy_df =dummy_df.drop_duplicates()
        items=dummy_df.values.tolist()

        # NORMALIZE ALL NAMES AND DOID VALUES TO DEFINED LIST
        tmp_dict = {}
        for index, item in tqdm(enumerate(items), ascii=True, desc="\tNormalizing disease names and DOID values with {} datapoints".format(len(items))):
            disease=item[0].lower()
            # disease='ovarian sex cord-stromal tumours'
            try:
                items[index][0]=tmp_dict[disease]
                items[index][1]=self._cancer_doid_stop[tmp_dict[disease]]
            except KeyError:
                lookup_name, original_name = self.normalize_to_doid(disease, None, disease)
                items[index][0]=lookup_name
                items[index][1]=self._cancer_doid_stop[lookup_name]
                tmp_dict.update({disease: lookup_name})

        # ADD DATAPOINTS: ROWS ITH

        # GET ABSTRACTS FOR LIST OF PMIDS (ALLOW DUPLICATE REQUESTS)
        prepared_data = self.getAbstracts(self._datasetLabels[url]['label'], pmidList=items)

        # LIST OF LISTS AS PANDAS DATAFRAME
        header = ['cancer','doid','pmid','text','relevance']
        tmp_df = pd.DataFrame(prepared_data, columns=header)
        tmp_df['AboutCancer'] = self._datasetLabels[url]['aboutCancer'] # NOT TRUE: DOID -1 MEAN NOT ABOUT CANCER
        tmp_df.insert(0, 'dataset', url)

        # SAVE TO DISK
        tmp_df.loc[tmp_df['doid'] == '-1', ['AboutCancer']] = 'No'
        tmp_df=tmp_df.drop_duplicates()
        # print(tmp_df.cancer.unique())
        # print(tmp_df.doid.unique())
        tmp_df.to_csv(self._storeData + url + '.csv', index=None, header=True, sep='\t')
        return tmp_df

    def prepare_pubmed(self, dataset='pubmed', delimiter='\t'):
        # papare data from pubmed, available on disk. no online lookup.
        header = ['cancer', 'doid', 'pmid', 'text', 'relevance']#, 'AboutCancer']
        all_docs = []
        p = pickle.load(open(self._originalData+'pubmed_recent.p', 'rb'))

        for item in p:
            pmid = item['MedlineCitation_PMID']
            all_docs.append([
                'No Cancer',
                '-1',
                pmid,
                item['Article_ArticleTitle'] + ' ' + item['Abstract_AbstractText'],
                'NotRelevant'
                ])

        df = pd.DataFrame(all_docs, dtype='str', columns=header)
        df['AboutCancer'] = self._datasetLabels[dataset]['aboutCancer']
        df.to_csv(self._storeData + dataset + '.csv', index=None,header=True, sep=delimiter)
        return df

    def load_dataset(self, dataset):

        # LOAD PREPARED DATASET FROM CSV FOR MULTI-TASK LEARNING!
        #  RETURN PANDAS DATAFRAME
        # INDEXED:  -3: PMID
        #           -2: TEXT
        #           -1: LABEL

        if dataset not in self._datasetLabels.keys():
            print("dataset needs to be on of: ", self._datasetLabels.keys())
            raise ValueError
        # print(self._storeData + dataset + '.csv')
        try:
            df = pd.read_csv(self._storeData + dataset+ '.csv', delimiter='\t', dtype='str')
        except (IOError, FileNotFoundError):
            if dataset != 'pubmed':
                df = self.prepare_csv(dataset)
            else:
                df = self.prepare_pubmed(dataset)
        return df

    def prepareForTraining(self, corpora, fileName, tokenizer=None, kerasTokenizer=None, labelizer=None, median=False):

        # PREPARE CORPORA FOR TRAINING FOR KERAS (TOKENIZATION, INDEXING ET AL).
        # corpora -> list of lists
        # fileName -> source or target
        # tokenizer -> split sentence to tokens
        # kerasTokenizer -> tokenizer for keras models and mappings
        # return: tokenizer, word->id mapping, id->word mapping, vocabulary

        fileName = self._storeUtilities+fileName+'.util'
        word_to_index_dict=None
        index_to_word_dict=None
        max_sentence_nr = None #SENTENCES IN DOCUMENT
        max_sentence_tokens_nr=None #TOKENS PER SENTENCE
        tokens = None

        try:

            with open(fileName, 'rb') as f:
                pData = pickle.load(f)

            kerasTokenizer = pData['tokenizer']
            word_to_index_dict = pData['w2i']
            index_to_word_dict = pData['i2w']
            max_sentence_tokens_nr= pData['max_sentence_tokens_nr'] #SENTENCE LEGNTH
            max_sentence_nr=pData['max_sentence_nr']
            tokens=pData['tokenized_data']
            # labelizer=pData['labelizer']
            print("Loading tokenized corpus for model {}".format(fileName))

        except OSError:
            print("Tokenizing corpus for dataset {}".format(fileName))
            tokens = []
            max_sentence_nr = 0
            max_sentence_tokens_nr = 0

            for item in corpora:
                sents = sentence_tokenize(item)
                max_sentence_nr = len(sents) if len(sents) > max_sentence_nr else max_sentence_nr

                token_sents = tokenizer.transform(sents)
                # print(token_sents)
                tokens_in_sents = max([len(x) for x in token_sents])
                max_sentence_tokens_nr = tokens_in_sents if tokens_in_sents > max_sentence_tokens_nr else max_sentence_tokens_nr
                tokens.append(token_sents)
                # print(tokens)
                # input('tokens: get median for max_sentcence_nr and max_sentence_tokens_nr for hatt')

            if median:
                # max_sentence_nr = int(median_stat([len(x) for x in tokens]))
                # max_sentence_tokens_nr =  int(median_stat([len(y) for x in tokens for y in x if y]))

                max_sentence_nr = int(np.percentile([len(x) for x in tokens], 75))
                max_sentence_tokens_nr = int(np.percentile([len(y) for x in tokens for y in x if y], 75))

            tmp = [item for item in list(set(morph.flatten(tokens))) if item.strip()]
            word_to_index_dict = {item.strip():i+1 for i, item in enumerate(tmp) if item.strip()}
            index_to_word_dict = {v: k for k, v in word_to_index_dict.items()}
            kerasTokenizer.word_index = word_to_index_dict

            with open(fileName, 'wb') as handle:
                pickle.dump({'tokenizer': kerasTokenizer,
                             'w2i': word_to_index_dict,
                             'i2w': index_to_word_dict,
                             'max_sentence_tokens_nr': max_sentence_tokens_nr,
                             'max_sentence_nr' : max_sentence_nr,
                             'tokenized_data': tokens,
                             # 'labelizer':labelizer
                             }, handle)

        return kerasTokenizer, word_to_index_dict, index_to_word_dict, max_sentence_nr, max_sentence_tokens_nr, tokens

    def get_embedding_layer(self, vocabulary, max_seq_length, model_name, layer_name=None):

        try:
            print("Loading stored embedding matrix for model {}".format(model_name))
            embedding_matrix = np.load(self._storeUtilities+model_name+'.matrix')
        except FileNotFoundError:
            print("No stored embedding matrix for model {} found. Creating from scratch.".format(model_name))
            if not self.embedding_model:
                self.load_embedding_model()

            if not self.use_fastText:
                EMBEDDING_DIM = self.embedding_model['stop'].shape[0]
                embedding_matrix = np.random.random((len(vocabulary) + 1, EMBEDDING_DIM))
                for word, i in vocabulary.items():
                    if i == 0:
                        input('cant be!')
                    try:
                        embedding_matrix[i]=self.embedding_model.get_vector(word)
                    except KeyError:
                        pass
                embedding_matrix[0]=np.zeros((1,EMBEDDING_DIM))

            else:
                # input('using fastText!')
                EMBEDDING_DIM = self.embedding_model.get_word_vector('stop').shape[0]
                embedding_matrix = np.zeros((len(vocabulary) + 1, EMBEDDING_DIM))
                for word, i in vocabulary.items():
                    if i == 0:
                        input('cant be!')
                    try:
                        embedding_matrix[i]=self.embedding_model.get_word_vector(word)
                    except KeyError as e:
                        print(e)
                        input('KeyError just cant be with FastText!')

            embedding_matrix.dump(self._storeUtilities+model_name+'.matrix')

        if not layer_name:
            layer_name = 'embeddings_layer'

        embedding_layer = Embedding(
            embedding_matrix.shape[0],
            embedding_matrix.shape[1],
            weights=[embedding_matrix],
            input_length=max_seq_length,
            trainable=False,
            mask_zero=True,
            name=layer_name
        )

        return embedding_matrix, embedding_layer

    def read_corpora(self, dataset, dataset_name, oversample_by_copy=False, oversample_by_parent=False):

        if oversample_by_copy and oversample_by_parent:
            raise ValueError("Oversampling either by copy or finding parent DOID. Currently set to both.")

        try:
            df = pd.read_csv(self._trainFolder+'{}.csv'.format(dataset_name), delimiter='\t', dtype='str')
        except FileNotFoundError:
            data = {}
            for corpus in dataset:
                data[corpus] = self.load_dataset(corpus)

            length = sum([len(v) for k,v in data.items() if k != 'pubmed'])
            data['pubmed']=data['pubmed'][:int(2*length)]

            df = pd.concat([v for k,v in data.items()]).drop_duplicates()
            print("Corpus before fuplicating with shape {}".format(df.shape))

            if oversample_by_parent:
                df = self.getDoid_Parent(df)

            if oversample_by_copy:
                df = self.oversample_duplicate(df)
            print("Corpus after duplicating with shape {}".format(df.shape))

            df.to_csv(self._trainFolder+'{}.csv'.format(dataset_name), index=None, header=True, sep='\t')

        df = sk_shuffle(df)
        return df

    def get_labels(self, labels, labelizer_name=None):
        # TRANSFORM LABELS FOR MULTI TASK
        print("Preparing labels for dataset \t {}".format(labelizer_name))

        try:
            labelizer = joblib.load(self._storeUtilities+labelizer_name+'.label')
        except Exception:
            label_encoder = LabelEncoder()
            labelizer = label_encoder.fit(labels)
            # labels = labelizer.transform(labels)
            # labels = to_categorical(np.asarray(labels))
            joblib.dump(labelizer, self._storeUtilities + labelizer_name + '.label')

        encoded_labels = labelizer.transform(labels)
        encoded_labels = to_categorical(np.asarray(encoded_labels))
        # print(labelizer_name, '\n', encoded_labels, '\n', encoded_labels.shape,'\n',  np.sum(encoded_labels, axis=0))
        print("Loaded labels for dataset \t {}".format(labelizer_name))
        return encoded_labels

    def prepare_corpora(self, task, dataset, use_median_length=False):

        tokenizer = TokenizePreprocessor()
        keras_tokenizer = Tokenizer()

        df = self.read_corpora(task, dataset)
        corpora = df.text.values
        labels = self.get_labels(df.iloc[:, self._tasks[task]['label_index']],
                                 task)

        keras_tokenizer, word_to_index_dict, index_to_word_dict, MAX_SENTS, MAX_SENT_LENGTH, tokens = \
            self.prepareForTraining(corpora, 'hatt_{}'.format(task), tokenizer, keras_tokenizer, median=use_median_length)

        data = np.zeros((len(tokens), MAX_SENTS, MAX_SENT_LENGTH), dtype='int32')
        # print(data.shape)
        for i, sentences in enumerate(tokens):
            for j, sent in enumerate(sentences):
                if j < MAX_SENTS:
                    for k, token in enumerate(sent):
                        if k < MAX_SENT_LENGTH:
                            data[i, j, k] = keras_tokenizer.word_index[token]

        embedding_matrix, embedding_layer = self.get_embedding_layer(keras_tokenizer.word_index, MAX_SENT_LENGTH)

        return data, labels, embedding_layer, MAX_SENT_LENGTH, MAX_SENTS

    def store_results(self, data, task, modelname, dataset, evaluated_on_dataset):
        # dataset: DEV OR TRAIN
        if evaluated_on_dataset not in ['dev', 'test']:
            raise ValueError("Evaluation dataset can be either 'dev' or 'test'")

        df = pd.DataFrame(data, columns=['f1', 'p', 'r'])
        df['task']=task
        df['model']=modelname
        df['dataset']=dataset
        df['evaluation_dataset']=evaluated_on_dataset

        # df.to_csv(self._resultsDir+modelname+'_'+evaluated_on_dataset+'.cvs',
        #           sep='\t',
        #           header=df.columns,
        #           index=False)

        return df