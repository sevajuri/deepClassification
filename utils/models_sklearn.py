from __future__ import division
import warnings

warnings.filterwarnings("ignore")

import itertools
import sys
import traceback
# from _config import *

# sklearn
from scipy.stats import randint as sp_randint, uniform
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression, SGDClassifier, Lasso, ElasticNet
from sklearn.metrics import classification_report, precision_recall_fscore_support, confusion_matrix
from sklearn.model_selection import RandomizedSearchCV, train_test_split, StratifiedKFold
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import cohen_kappa_score, accuracy_score, average_precision_score
# from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_selection import SelectPercentile, chi2
from collections import Counter
import time
import pandas as pd

from Bio import Entrez

Entrez.email = "seva@informatik.hu-berlin.de"

from utils.transformers import *

from multiprocessing import cpu_count


class getBestModel:

    def __init__(self, modelName, data, labels, dataset, task, testData=None, testLabels=None, modelFolder='results/',
                 n_jobs=cpu_count() - 5):
        # self.getFastTextWE()
        self.trainingData = []
        self.trainingDataLabels = []
        self.chapters = []
        self.blocks = []
        self.data = data
        self.labels = labels

        self.testData = testData
        self.testLabels = testLabels

        self.n_iter_search = 10
        self.n_jobs = n_jobs
        self.randIntMax = 1000
        self.modelName = modelName

        self._storeDir = './trained_models/'
        self._storeUtilities = self._storeDir + 'utilityModels/'
        self._resultsDir = 'results/'

        self.dataset = dataset
        self.task = task

        self.toknizer = TokenizePreprocessor()

        self.estimators = [
            RandomForestClassifier(),
            # DecisionTreeClassifier(),
            # LogisticRegression(),
            # SGDClassifier(),
            SVC(),
            # MultinomialNB(),
            # MLPClassifier()
        ]
        self.names = [
            "RandomForestClassifier",
            # "DecisionTreeClassifier",
            # "LogisticRegression",
            # "SGDClassifier",
            "SVM_SVC",
            # "MultinomialNB",
            # "MLPClassifier"
        ]

        self.estimatRandomSearchParameters = {
            "LogisticRegression": {
                'C': sp_randint(1, self.randIntMax),
                'solver': ['newton-cg', 'lbfgs', 'liblinear'],
                'class_weight': ['balanced']
            },
            "SVM_SVC": {
                'C': sp_randint(1, self.randIntMax),
                'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
                'class_weight': ['balanced'],
                'probability': [True]
            },
            "SVM_LinearSVC": {
                'C': sp_randint(1, self.randIntMax),
                'class_weight': ['balanced']
            },
            "DecisionTreeClassifier": {
                "criterion": ["gini", "entropy"],
                "splitter": ["best", "random"],
                'max_depth': sp_randint(1, 1000),
                'class_weight': ['balanced']
            },
            "RandomForestClassifier": {
                'n_estimators': sp_randint(1, self.randIntMax),
                "criterion": ["gini", "entropy"],
                'max_depth': sp_randint(1, self.randIntMax),
                'class_weight': ['balanced']
            },
            "KNeighbors": {
                'n_neighbors': sp_randint(1, 40),
                'weights': ['uniform', 'distance'],
                'algorithm': ['auto'],
                'leaf_size': sp_randint(1, self.randIntMax),
                'class_weight': ['balanced']
            },
            "SGDClassifier": {
                'loss': ['hinge', 'log', 'modified_huber', 'squared_hinge'],
                'class_weight': ['balanced'],
                'penalty': ['l2', 'l1', 'elasticnet'],
                'learning_rate': ['optimal', 'invscaling'],
                'eta0': uniform(0.01, 0.00001)
            },
            "MultinomialNB": {
                'alpha': uniform(0, 1),
                'fit_prior': [True],
            },
            "LinearSVC": {
                'C': uniform(0, 1),
                'loss': ['hinge', 'squared_hinge']
            },
            "Lasso": {
                'alpha': uniform(0, 1),
                'fit_intercept': [True],
                'normalize': [True, False],
                'max_iter': sp_randint(1, self.randIntMax)
            },
            "ElasticNet": {
                'alpha': uniform(0, 1),
                'l1_ratio': uniform(0, 1)
            },
            "MLPClassifier": {
                'activation': ['identity', 'logistic', 'tanh', 'relu'],
                'solver': ['lbfgs', 'sgd', 'adam'],
                'learning_rate': ['constant', 'invscaling', 'adaptive'],
                'max_iter': [100000]
            }
        }

    #
    # CLASS BASED ACCURACY
    #
    def indices(self, l, val):
        retval = []
        last = 0
        while val in l[last:]:
            i = l[last:].index(val)
            retval.append(last + i)
            last += i + 1
        return retval

    #
    # CLASS BASED ACCURACY
    #
    def class_accuracy(self, y_pred, y_true, className):

        index = self.indices(y_true, className)
        tp = [x for x in index if y_pred[x] == className]
        # print y_true[:20]
        # print y_pred[:20]
        # print index,tp
        tp = len(tp)
        # print tp, len(y_pred)
        return float(tp / len(index))

    # def createFeatureVector(self, dataSet):
    #
    #     print('Extracting features')
    #     dummyDS=list()
    #
    #     for i, row in enumerate(dataSet):
    #         dummyDS.append(list(itertools.chain(*[self.embeddingModels['EN'][row], self.embeddingModels['FR'][row] ])))
    #         #dummyDS.append(np.average([self.embeddingModels['EN'][row], self.embeddingModels['FR'][row] ]))
    #     return dummyDS

    def buildVocabulary(self, ngrams):
        # =======================================================================
        # create feautres per label, count disticnt features per label, concatenate to a feature vocab and use that to perform TfidfVectorizer
        # =======================================================================

        ct = CountVectorizer(ngram_range=ngrams, lowercase=True, strip_accents='unicode', token_pattern=u'\S+[^.,!?\s]')

        yes = []
        no = []

        for a, b in zip(self.data, self.labels):
            if b == 1:
                yes.append(a)

            if b == 0:
                no.append(a)

        yesCT = ct.fit(yes).vocabulary_.keys()
        noCT = ct.fit(no).vocabulary_.keys()

        onlyYes = list(set(yesCT) - set(noCT))
        onlyNo = list(set(noCT) - set(yesCT))

        return onlyNo + onlyYes

    def dummy_tokenizer(self, doc):
        return doc

    def prepareTrainTest(self, ngramRange, trainData, testData, trainLabels, max_df_freq, analyzerLevel='word',
                         featureSelect=True, vocab=None):

        print("Vectorizing and stuff")
        transformedTrainData = None
        transformedTestData = None
        tfidfVect = TfidfVectorizer(ngram_range=ngramRange,
                                    analyzer=analyzerLevel,
                                    norm='l2',
                                    decode_error='replace',
                                    max_df=max_df_freq,
                                    sublinear_tf=True,
                                    lowercase=True,
                                    strip_accents='unicode',
                                    tokenizer=self.dummy_tokenizer,
                                    preprocessor=self.dummy_tokenizer,
                                    vocabulary=vocab,
                                    # token_pattern=None
                                    )

        try:
            transformedTrainData = tfidfVect.fit_transform(trainData)
            transformedTestData = tfidfVect.transform(testData)
        except TypeError as e:

            transformedTrainData = tfidfVect.fit_transform(trainData)
            transformedTestData = tfidfVect.transform(testData)
            print(e)
            traceback.print_exc()

        ch2 = None
        if featureSelect:
            print("Selecting best features")
            ch2 = SelectPercentile(chi2, 20)
            transformedTrainData = ch2.fit_transform(transformedTrainData, trainLabels)
            transformedTestData = ch2.transform(transformedTestData)

        # print 'Transformed train data set feature space size:\tTrain {}\t\t Test{}'.format(transformedTrainData.shape, transformedTestData.shape)
        return transformedTrainData, transformedTestData, tfidfVect, ch2

    def getBestModel(self):

        results = []
        resultsRelevant = []
        max_df_frequency = [1.0]

        bestResult = {
            'word': {
                'best': 0,
                'random_search_': None,
                'best_ngram': None,
                'best_chi2': None,
                'model': None,
                'analyzer': [(1, 2), (1, 3), (1, 4)]
            },
            # 'char':{
            #         'best':0,
            #         'p':0,
            #         'f':0,
            #         'r':0,
            #         'acc':0,
            #         'random_search_':None,
            #         'best_ngram':None,
            #         'best_chi2':None,
            #         'model':None,
            #         'analyzer':[(1,4),(2,4), (2,5), (2,6)]
            #     }
        }

        serialize = {
            'best': 0,
            'p': 0,
            'f': 0,
            'r': 0,
            'acc': 0,
            'random_search_': None,
            'best_ngram': None,
            'best_chi2': None,
            'model': None,
        }

        TrainData, TestData, y_train, y_test = train_test_split(self.data, self.labels, test_size=0.2, random_state=42,
                                                                stratify=self.labels)
        print('Splitting training data in dev/eval sets:\tDev {}\tEval{}'.format(Counter(y_train), Counter(y_test)))

        for k, v in bestResult.items():
            for ngram in v['analyzer']:

                transformedTestData = None
                X_train = None
                X_test = None
                tfidf = None
                ch2 = None
                random_search = None

                X_train, X_test, tfidf, ch2 = self.prepareTrainTest(ngram, TrainData, TestData, y_train, 1.0, k)

                if self.testData:
                    if ch2:
                        transformedTestData = ch2.transform(tfidf.transform(self.testData))
                        transformedFoldData = ch2.transform(tfidf.transform(self.data))
                    else:
                        transformedTestData = tfidf.transform(self.testData)
                        transformedFoldData = tfidf.transform(self.data)

                for i, model in enumerate(self.estimators):

                    print('{}, {}, {}, {}, ngram({},{})'.format(self.task, self.dataset, self.names[i], k, ngram[0],
                                                                ngram[1]))
                    random_search = RandomizedSearchCV(model,
                                                       param_distributions=self.estimatRandomSearchParameters[
                                                           self.names[i]],
                                                       n_iter=self.n_iter_search,
                                                       n_jobs=self.n_jobs,
                                                       pre_dispatch='n_jobs',
                                                       cv=5,
                                                       scoring='f1_macro',
                                                       verbose=0)
                    random_search.fit(X_train, y_train)
                    folds = list(StratifiedKFold(n_splits=5, shuffle=True, random_state=42).split(transformedFoldData,
                                                                                                  self.labels))
                    for j, (train_idx, val_idx) in enumerate(folds):
                        print('\nFold ', j)
                        X_train_cv = transformedFoldData[train_idx]
                        y_train_cv = self.labels[train_idx]

                        X_valid_cv = transformedFoldData[val_idx]
                        y_valid_cv = self.labels[val_idx]

                        # ===============================================================
                        # EVALUATE ON EVAL DS
                        # ===============================================================
                        fold_model = random_search.best_estimator_.fit(X_train_cv, y_train_cv)
                        y_predicted = fold_model.predict(X_valid_cv)
                        p, r, f, s = precision_recall_fscore_support(y_valid_cv, y_predicted, pos_label=None,
                                                                     average='weighted')
                        # acc = accuracy_score(y_test, y_predicted)
                        print("\tTEST P: {}\tR:{}\tF1{}".format(p, r, f))
                        results.append([f,
                                        p,
                                        r,
                                        self.task,
                                        self.names[i],
                                        self.dataset,
                                        'test',
                                        k,
                                        '({},{})'.format(ngram[0], ngram[1]),
                                        ])

                        # list(StratifiedKFold(n_splits=5, shuffle=True, random_state=42).split(x_train, y_train.argmax(1)))
                        # ===============================================================
                        # EVALUATE ON TEST DS
                        # ===============================================================
                        y_predicted = fold_model.predict(transformedTestData)
                        p, r, f, s = precision_recall_fscore_support(self.testLabels, y_predicted, average='weighted')
                        acc = accuracy_score(self.testLabels, y_predicted)
                        print("\tDEV P: {}\tR:{}\tF1{}".format(p, r, f))
                        results.append([f,
                                        p,
                                        r,
                                        self.task,
                                        self.names[i],
                                        self.dataset,
                                        'dev',
                                        k,
                                        '({},{})'.format(ngram[0], ngram[1]),
                                        ])

                        # ===========================================================
                        # BEST MODEL INFO
                        # ===========================================================
                        if f > bestResult[k]['best']:
                            bestResult[k]['model'] = self.names[i]
                            bestResult[k]['best'] = f
                            bestResult[k]['acc'] = acc
                            bestResult[k]['random_search_'] = random_search
                            bestResult[k]['best_ngram'] = tfidf
                            bestResult[k]['best_chi2'] = ch2

                        if f > serialize['f']:
                            serialize['model'] = self.names[i]
                            serialize['best'] = f
                            serialize['acc'] = acc
                            serialize['random_search_'] = random_search.best_estimator_
                            serialize['best_ngram'] = tfidf
                            serialize['best_chi2'] = ch2

        joblib.dump(serialize['random_search_'], self._storeDir + '{}_{}_sklearn.model'.format(self.task, self.dataset))
        joblib.dump(serialize['best_ngram'],
                    self._storeUtilities + '{}_{}_sklearn.tfidf'.format(self.task, self.dataset))
        if serialize['best_chi2']:
            joblib.dump(serialize['best_chi2'],
                        self._storeUtilities + '{}_{}_sklearn.chi2'.format(self.task, self.dataset))

        header = ['f1', 'p', 'r', 'task', 'model', 'dataset', 'evaluation_dataset', 'feature_level', 'n_gram']
        df = pd.DataFrame(results)
        df.to_csv(self._resultsDir + '{}_sklearn.csv'.format(self.modelName), index=False, header=header,
                  sep="\t")
