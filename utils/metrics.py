import numpy as np
from keras.callbacks import Callback
from sklearn.metrics import roc_auc_score, f1_score, precision_score, recall_score, auc
from keras import backend as K


class Metrics(Callback):

    def on_train_begin(self, tasks=None, logs={}):
        # tasks = dict with names
        self.val_f1s = []
        self.val_recalls = []
        self.val_precisions = []
        self.aucs = []
        self.losses = []
        self.final_evaluation = []


    # def on_epoch_end(self, epoch, logs={}):
    #
    #     predictions = self.model.predict(self.validation_data[0])
    #
    #     if type(predictions) is np.ndarray:
    #
    #         val_targ = np.argmax(self.validation_data[1], axis=1)
    #         val_predict = np.argmax(predictions, axis=1)
    #
    #         _val_f1 = f1_score(val_targ, val_predict, average='weighted')
    #         _val_recall = recall_score(val_targ, val_predict, average='weighted')
    #         _val_precision = precision_score(val_targ, val_predict, average='weighted')
    #         # _val_auc = roc_auc_score(val_targ, val_predict)
    #
    #         self.losses.append(logs.get('loss'))
    #         self.val_f1s.append(_val_f1)
    #         self.val_recalls.append(_val_recall)
    #         self.val_precisions.append(_val_precision)
    #         # self.aucs.append(_val_auc)
    #
    #         # print(" — val_f1: {} — val_precision: {} — val_recall {}".format(_val_f1, _val_precision, _val_recall))
    #
    #
    #     elif isinstance(predictions, (list,)):
    #
    #         tmp_f1 = []
    #         tmp_p = []
    #         tmp_r = []
    #         tmp_auc = []
    #
    #         for index in range(0, len(predictions)):
    #             val_targ = np.argmax(self.validation_data[1+index], axis=1)
    #             val_predict = np.argmax(predictions[index], axis=1)
    #
    #             _val_f1 = f1_score(val_targ, val_predict, average='weighted')
    #             _val_recall = recall_score(val_targ, val_predict, average='weighted')
    #             _val_precision = precision_score(val_targ, val_predict, average='weighted')
    #             # _val_auc = roc_auc_score(val_targ, val_predict)
    #
    #             tmp_f1.append(_val_f1)
    #             tmp_p.append(_val_precision)
    #             tmp_r.append(_val_recall)
    #             # tmp_auc.append(_val_auc)
    #
    #         self.losses.append(logs.get('loss'))
    #         self.val_f1s.append(tmp_f1)
    #         self.val_recalls.append(tmp_r)
    #         self.val_precisions.append(tmp_p)
    #         self.aucs.append(tmp_auc)
    #         # print(" — val_f1: {} — val_precision: {} — val_recall {}".format(tmp_f1, tmp_p, tmp_r))
    #
    #     else:
    #         raise ValueError('Brotha, your types all are fucked up. Check Metrics, on_epoch_end')
    #
    #     return

    def on_train_end(self, logs=None):

        predictions = self.model.predict(self.validation_data[0])

        if type(predictions) is np.ndarray:
            val_targ = np.argmax(self.validation_data[1], axis=1)
            val_predict = np.argmax(predictions, axis=1)
            _val_f1 = f1_score(val_targ, val_predict, average='weighted')
            _val_recall = recall_score(val_targ, val_predict, average='weighted')
            _val_precision = precision_score(val_targ, val_predict, average='weighted')
            self.final_evaluation = [_val_f1, _val_precision, _val_recall]

        elif isinstance(predictions, (list,)):

            for index in range(0, len(predictions)):
                val_targ = np.argmax(self.validation_data[1+index], axis=1)
                val_predict = np.argmax(predictions[index], axis=1)
                _val_f1 = f1_score(val_targ, val_predict, average='weighted')
                _val_recall = recall_score(val_targ, val_predict, average='weighted')
                _val_precision = precision_score(val_targ, val_predict, average='weighted')
                self.final_evaluation.append([_val_f1, _val_precision, _val_recall])
        else:
            raise ValueError('Brotha, your types all are fucked up. Check Metrics, on_epoch_end')

        return


def precision(y_true, y_pred):
    """Precision metric.

    Only computes a batch-wise average of precision.

    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.

    Only computes a batch-wise average of recall.

    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def fbeta_score(y_true, y_pred, beta=1):
    """Computes the F score.

    The F score is the weighted harmonic mean of precision and recall.
    Here it is only computed as a batch-wise average, not globally.

    This is useful for multi-label classification, where input samples can be
    classified as sets of labels. By only using accuracy (precision) a model
    would achieve a perfect score by simply assigning every class to every
    input. In order to avoid this, a metric should penalize incorrect class
    assignments as well (recall). The F-beta score (ranged from 0.0 to 1.0)
    computes this, as a weighted mean of the proportion of correct class
    assignments vs. the proportion of incorrect class assignments.

    With beta = 1, this is equivalent to a F-measure. With beta < 1, assigning
    correct classes becomes more important, and with beta > 1 the metric is
    instead weighted towards penalizing incorrect class assignments.
    """
    if beta < 0:
        raise ValueError('The lowest choosable beta is zero (only precision).')

    # If there are no true positives, fix the F score at 0 like sklearn.
    if K.sum(K.round(K.clip(y_true, 0, 1))) == 0:
        return 0

    p = precision(y_true, y_pred)
    r = recall(y_true, y_pred)
    bb = beta ** 2
    fbeta_score = (1 + bb) * (p * r) / (bb * p + r + K.epsilon())
    return fbeta_score
