from keras import backend as K, objectives
from keras.layers import Bidirectional, Dense, Embedding, Input, Lambda, LSTM, GRU, RepeatVector, TimeDistributed, Dropout
from keras.models import Model, load_model as keras_load_model
from keras.callbacks import LearningRateScheduler, EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard, CSVLogger
from keras.utils.multi_gpu_utils import multi_gpu_model

from utils.data_loader import data_loader
from utils.layers import AttLayer, Attention
from utils.optimizers import AdamW, SGDW
from utils.metrics import Metrics

from sklearn.model_selection import train_test_split, KFold, StratifiedKFold
from sklearn.metrics import roc_auc_score, f1_score, precision_score, recall_score, auc

import numpy as np
import pandas as pd
import keras_metrics

from keras.optimizers import Adam, SGD
from keras.initializers import RandomNormal

###################################
# TensorFlow wizardry
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type = 'BFC'
config.allow_soft_placement=True
sess = tf.Session(config=config)

# Create a session with the above options specified.
K.tensorflow_backend.set_session(tf.Session(config=config))

class Models():

    def __init__(self):

        # other stuff
        self._storeDir='./trained_models/'
        self._storeUtilities = './trained_models/utilityModels'
        self._resultsDir = 'results/'
        self._tBoardDir = './tboard'
        self._callback_list = None
        self.data_loader = data_loader()
        self._adamw = AdamW(lr=0.001, beta_1=0.9, beta_2=0.999, weight_decay=1e-4, epsilon=1e-8, decay=0.)
        self._sgdw = SGDW(lr=0.01, momentum=0., decay=0., weight_decay=1e-4, nesterov=False)
        self.best_f = 0.0
        # self._loader = data_loader()

    def step_decay_schedule(self, initial_lr=1e-3, decay_factor=0.75, step_size=2):
    # Wrapper function to create a LearningRateScheduler with step decay schedule.

        def schedule(epoch):
            return initial_lr * (decay_factor ** np.floor(epoch / step_size))

        return LearningRateScheduler(schedule)

    def get_callbacks(self, modelname, dataset, experiment_id=None):

        if experiment_id:
            fileName = '{}_{}'.format(modelname, dataset, experiment_id)
        else:
            fileName = modelname

        metrics = Metrics()

        callback_list = [
            EarlyStopping(
                monitor='val_loss',
                patience=3,
                min_delta=0.001
            ),
            # ModelCheckpoint(
            #     filepath='{}/{}.h5'.format(self._storeDir, fileName),
            #     monitor='val_loss',
            #     save_best_only=True,
            # ),
            CSVLogger(
                append=False,
                filename='{}/logs/{}.csv'.format(self._resultsDir, fileName)
             ),
            ReduceLROnPlateau(
                monitor='val_loss',
                factor=0.5,
                patience=2,
                min_lr=1e-5,
                verbose=0
            ),
            # self.step_decay_schedule(
            #     initial_lr=1e-2,
            #     decay_factor=0.75,
            #     step_size=1
            # ),
            TensorBoard(
                    log_dir=self._tBoardDir,
                    histogram_freq=0,
                    write_graph=True,
                    write_images=True
            ),
            metrics
        ]

        return callback_list

    def basic_model(self, embedding_layer, MAX_SENTS, MAX_SENT_LENGTH, NR_LABELS,  multilabel=False):

        sentence_input = Input(shape=(MAX_SENT_LENGTH,), dtype='int32')
        embedded_sequences = embedding_layer(sentence_input)
        # l_lstm = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(embedded_sequences)
        l_lstm = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(embedded_sequences)
        l_att = Attention()(l_lstm)
        sentEncoder = Model(sentence_input, l_att)
        # sentEncoder.summary()

        review_input = Input(shape=(MAX_SENTS, MAX_SENT_LENGTH), dtype='int32')
        review_encoder = TimeDistributed(sentEncoder)(review_input)
        # l_lstm_sent = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(review_encoder)
        l_lstm_sent = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(review_encoder)
        l_att_sent = Attention()(l_lstm_sent)
        prepreds = Dense(50)(l_att_sent)
        if multilabel:
            preds = Dense(NR_LABELS, activation='sigmoid', name='prediction')(prepreds)
        else:
            preds = Dense(NR_LABELS, activation='softmax', name='prediction')(prepreds)
        model = Model(review_input, preds)
        # model.summary()
        return model

    def save_best(self, f_score):
        if np.sum(f_score) > self.best_f:
            print("Saving best model")
            self.best_f = np.sum(f_score)
            return True
        return False

    def get_splits(self, x_train, y_train):
        return list(StratifiedKFold(n_splits=5, shuffle=True, random_state=42).split(x_train, y_train.argmax(1)))

    def hatt(self,
             data,
             labels,
             embedding_layer,
             modelname,
             task,
             dataset,
             experiment_id=None,
             batch_size=50,
             epochs=30):

        x_train, x_val, y_train, y_val = train_test_split(data, labels,
                                                          test_size=0.15,
                                                          stratify=labels)

        print('Labels: Train {} \t Test{}'.format(y_train.sum(axis=0), y_val.sum(axis=0)))
        print('Shapes: Train {} {} \t Test {} {}'.format(x_train.shape, y_train.shape, x_val.shape, y_val.shape))

        # IMPLEMENT K-FOLD CV, PER ULFS REQUEST
        # folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=42).split(x_train, y_train.argmax(1)))
        folds = self.get_splits(x_train, y_train)

        results_test = []
        results_dev = []

        print("Model Fitting - Hierarchical attention network")
        for j, (train_idx, val_idx) in enumerate(folds):
            print('\nFold ', j)
            X_train_cv = x_train[train_idx]
            y_train_cv = y_train[train_idx]
            X_valid_cv = x_train[val_idx]
            y_valid_cv = y_train[val_idx]

            # print(X_train_cv, X_train_cv.shape)
            # print(y_train_cv, y_train_cv.shape)

            callback_list = self.get_callbacks(modelname, experiment_id)
            model = self.basic_model(embedding_layer, X_train_cv.shape[1], X_train_cv.shape[2], y_train_cv.shape[1])

            model.compile(loss='categorical_crossentropy',
                      optimizer=self._adamw,
                      metrics=['acc'])

            model.fit(X_train_cv, y_train_cv,
                      validation_data=(X_valid_cv, y_valid_cv),
                      nb_epoch=epochs,
                      batch_size=batch_size,
                      callbacks=callback_list,
                      verbose=0
                    )

            # print("{}".format(callback_list[-1].final_evaluation))
            print('Values on TEST (@last epoch): F1: {} - Precision: {} - Recall: {}'.format(callback_list[-1].final_evaluation[0],
                                                                                 callback_list[-1].final_evaluation[1],
                                                                                 callback_list[-1].final_evaluation[2]))
            results_test.append([
                callback_list[-1].final_evaluation[0],
                callback_list[-1].final_evaluation[1],
                callback_list[-1].final_evaluation[2]
            ])

            predictions = model.predict(x_val)
            val_targ = np.argmax(y_val, axis=1)
            val_predict = np.argmax(predictions, axis=1)

            _val_f1 = f1_score(val_targ, val_predict, average='weighted')
            _val_recall = recall_score(val_targ, val_predict, average='weighted')
            _val_precision = precision_score(val_targ, val_predict, average='weighted')
            results_dev.append([_val_f1, _val_precision, _val_recall])

            print('Predictions on DEV: F1: {} - Precision: {} - Recall: {}'.format(_val_f1,_val_precision, _val_recall))

            if self.save_best(_val_f1):
                model.save('{}/{}.h5'.format(self._storeDir, modelname))

        results_store = []
        results_store.append(self.data_loader.store_results(results_dev, task, 'HATT', dataset, 'dev'))
        results_store.append(self.data_loader.store_results(results_test, task, 'HATT', dataset, 'test'))

        df_store = pd.concat(results_store)
        df_store.to_csv(self._resultsDir+modelname+'.csv', sep='\t', index=False, header =df_store.columns)

        f1, p, r = np.mean(results_test, axis=0)
        f1_dev, p_dev, r_dev = np.mean(results_dev, axis=0)

        print("Average perfromance on {} TEST DATASET: F1: {} \t P: {}\t R:{}".format(modelname, f1, p, r))
        print("Average perfromance on {} DEV DATASET: F1: {} \t P: {}\t R:{}".format(modelname, f1_dev, p_dev, r_dev))
        # input('All right mate?')

    def multi_task_model(self,
                         data,
                         labels_cancer_type,
                         labels_is_cancer,
                         labels_clinical,
                         embedding_layer,
                         modelname,
                         dataset,
                         experiment_id=None,
                         batch_size=50,
                         epochs=100):
        # MULTI-TASK LEARNING FOR IS_CANCER; WHICH_CANCER; IS_CLINICAL

        callback_list = self.get_callbacks(modelname, experiment_id)

        data_train, data_test, \
        labels_cancer_type_train, labels_cancer_type_test, \
        labels_is_cancer_train, labels_is_cancer_test, \
        labels_clinical_train, labels_clinical_test = \
            train_test_split(data,
                             labels_cancer_type,
                             labels_is_cancer,
                             labels_clinical,
                             test_size=0.15)

        # folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=42).split(data_train, labels_cancer_type_train.argmax(1)))
        folds = self.get_splits(data_train, labels_cancer_type_train)

        results_test = []
        results_dev = []
        validation_data = [labels_is_cancer_test, labels_cancer_type_test, labels_clinical_test]
        print("Model Fitting - Vanilla Multi-task learning for document classification")

        for j, (train_idx, val_idx) in enumerate(folds):

            print('\nFold ', j)
            X_train_cv = data_train[train_idx]
            X_valid_cv = data_train[val_idx]
            
            labels_is_cancer_train_cv = labels_is_cancer_train[train_idx]
            labels_is_cancer_valid_cv = labels_is_cancer_train[val_idx]

            labels_cancer_type_train_cv = labels_cancer_type_train[train_idx]
            labels_cancer_type_valid_cv = labels_cancer_type_train[val_idx]

            labels_clinical_train_cv = labels_clinical_train[train_idx]
            labels_clinical_valid_cv = labels_clinical_train[val_idx]


            # INPUT AND SHARED LAYER
            sentence_input = Input(shape=(X_train_cv.shape[1],), dtype='int32')
            embedded_sequences = embedding_layer(sentence_input)
            l_lstm = Bidirectional(GRU(256, return_sequences=True, dropout=0.2, activation='elu'))(embedded_sequences)
            # l_att = Attention()(l_lstm)

            # model1: IS CANCER
            m1 = GRU(100, dropout=0.4, activation='elu')(l_lstm)
            m1 = Dense(100, activation='elu')(m1)
            m1_out = Dense(labels_is_cancer_train.shape[1], activation='softmax', name='prediction_1')(m1)

            # model2: WHICH CANCER
            m2 = GRU(100, dropout=0.4, activation='elu')(l_lstm)
            m2 = Dense(100, activation='elu')(m2)
            m2_out = Dense(labels_cancer_type_train.shape[1], activation='softmax', name='prediction_2')(m2)

            # model3: IS CLINICAL
            m3 = GRU(100, dropout=0.4, activation='elu')(l_lstm)
            m3 = Dense(100, activation='elu')(m3)
            m3_out = Dense(labels_clinical_train.shape[1], activation='softmax', name='prediction_3')(m3)

            # MODEL
            mtl_model = Model(sentence_input, [m1_out, m2_out, m3_out])
            # mtl_model.summary()

            mtl_model.compile(loss=self.multi_loss,
                              optimizer=AdamW(lr=0.001, beta_1=0.9, beta_2=0.999, weight_decay=1e-4, epsilon=1e-8, decay=0.),
                              metrics=['acc'])

            mtl_model.fit(
                    X_train_cv,
                    [labels_is_cancer_train_cv,
                     labels_cancer_type_train_cv,
                     labels_clinical_train_cv],
                    validation_data=(X_valid_cv,
                           [labels_is_cancer_valid_cv,
                            labels_cancer_type_valid_cv,
                            labels_clinical_valid_cv]),
                    nb_epoch=epochs,
                    batch_size=batch_size,
                    callbacks=callback_list,
                    verbose=0
            )

            results_test.append([
                callback_list[-1].final_evaluation[0],
                callback_list[-1].final_evaluation[1],
                callback_list[-1].final_evaluation[2]
            ])

            print('Values at last epoch on TEST:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}'.format(
                callback_list[-1].final_evaluation[0],
                callback_list[-1].final_evaluation[1],
                callback_list[-1].final_evaluation[2]))

            # DEV DATA SET
            predictions = mtl_model.predict(data_test)
            task_results = []
            for index in range(0, len(predictions)):
                val_targ = np.argmax(validation_data[index], axis=1)
                val_predict = np.argmax(predictions[index], axis=1)
                _val_f1 = f1_score(val_targ, val_predict, average='weighted')
                _val_recall = recall_score(val_targ, val_predict, average='weighted')
                _val_precision = precision_score(val_targ, val_predict, average='weighted')
                task_results.append([_val_f1, _val_precision, _val_recall])
            results_dev.append(task_results)

            print('Predictions on DEV: \n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}'.format(task_results[0],
                                                                                              task_results[1],
                                                                                              task_results[2]))

            if self.save_best(_val_f1):
                mtl_model.save('{}/{}.h5'.format(self._storeDir, modelname))

        results_store = []
        results_store.append(self.data_loader.store_results([x[0] for x in results_dev], 'is_cancer', 'MTL', dataset, 'dev'))
        results_store.append(self.data_loader.store_results([x[1] for x in results_dev], 'cancer_type', 'MTL', dataset, 'dev'))
        results_store.append(self.data_loader.store_results([x[2] for x in results_dev], 'clinical', 'MTL', dataset, 'dev'))

        results_store.append(self.data_loader.store_results([x[0] for x in results_test], 'is_cancer', 'MTL', dataset, 'test'))
        results_store.append(self.data_loader.store_results([x[1] for x in results_test], 'cancer_type', 'MTL', dataset, 'test'))
        results_store.append(self.data_loader.store_results([x[2] for x in results_test], 'clinical', 'MTL', dataset, 'test'))

        df_store = pd.concat(results_store)
        df_store.to_csv(self._resultsDir+modelname+'.csv', sep='\t', index=False, header =df_store.columns)

        f1, p, r = np.mean(results_test, axis=0)
        f1_dev, p_dev, r_dev = np.mean(results_dev, axis=0)

        print("Average perfromance on {} TEST DATASET:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL:".format(modelname, f1, p, r))
        print("Average perfromance on {} DEV DATASET:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL:".format(modelname, f1_dev, p_dev, r_dev))
        # input('All right mate?')

    def multi_task_model_hatt(self,
                         data,
                         labels_cancer_type,
                         labels_is_cancer,
                         labels_clinical,
                         embedding_layer,
                         modelname,
                         dataset,
                         experiment_id=None,
                         batch_size=50,
                         epochs=100):
        # MULTI-TASK LEARNING FOR IS_CANCER; WHICH_CANCER; IS_CLINICAL

        callback_list = self.get_callbacks(modelname, experiment_id)

        data_train, data_test, labels_cancer_type_train, labels_cancer_type_test, \
        labels_is_cancer_train, labels_is_cancer_test, labels_clinical_train, labels_clinical_test = \
            train_test_split(data, labels_cancer_type, labels_is_cancer, labels_clinical,
                             test_size=0.15)

        # INPUT DIMENSIONS
        # input(data_train.shape)
        MAX_SENTS = data_train.shape[1]
        MAX_SENT_LENGTH = data_train.shape[2]

        # folds = list(StratifiedKFold(n_splits=10, shuffle=True, random_state=42).split(data_train, labels_cancer_type_train.argmax(1)))
        folds = self.get_splits(data_train, labels_cancer_type_train)

        results_test = []
        results_dev = []
        validation_data = [labels_is_cancer_test, labels_cancer_type_test, labels_clinical_test]
        print("Model Fitting - HATT Multi-task learning for document classification")

        for j, (train_idx, val_idx) in enumerate(folds):

            print('\nFold ', j)
            X_train_cv = data_train[train_idx]
            X_valid_cv = data_train[val_idx]

            labels_cancer_type_train_cv = labels_cancer_type_train[train_idx]
            labels_cancer_type_valid_cv = labels_cancer_type_train[val_idx]

            labels_is_cancer_train_cv = labels_is_cancer_train[train_idx]
            labels_is_cancer_valid_cv = labels_is_cancer_train[val_idx]

            labels_clinical_train_cv = labels_clinical_train[train_idx]
            labels_clinical_valid_cv = labels_clinical_train[val_idx]


            # INPUT AND SHARED LAYER
            sentence_input = Input(shape=(MAX_SENT_LENGTH,), dtype='int32')
            embedded_sequences = embedding_layer(sentence_input)
            # l_lstm = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(embedded_sequences)
            l_lstm = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(embedded_sequences)
            l_att = Attention()(l_lstm)
            sentEncoder = Model(sentence_input, l_att)
            # sentEncoder.summary()

            # INPUT
            review_input = Input(shape=(MAX_SENTS, MAX_SENT_LENGTH), dtype='int32')

            # model1: IS CANCER
            m1_review_encoder = TimeDistributed(sentEncoder)(review_input)
            # l_lstm_sent = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(review_encoder)
            m1_l_lstm_sent = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(m1_review_encoder)
            m1_l_att_sent = Attention()(m1_l_lstm_sent)
            m1_prepreds = Dense(50)(m1_l_att_sent)
            m1_out = Dense(labels_is_cancer_train.shape[1], activation='softmax', name='prediction_IS_CANCER')(m1_prepreds)

            # # model2: WHICH CANCER
            m2_review_encoder = TimeDistributed(sentEncoder)(review_input)
            # l_lstm_sent = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(review_encoder)
            m2_l_lstm_sent = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(m2_review_encoder)
            m2_l_att_sent = Attention()(m2_l_lstm_sent)
            m2_prepreds = Dense(50)(m2_l_att_sent)
            m2_out = Dense(labels_cancer_type_train.shape[1], activation='softmax', name='prediction_cancer_type')(m2_prepreds)

            # model3: IS CLINICAL
            m3_review_encoder = TimeDistributed(sentEncoder)(review_input)
            # l_lstm_sent = Bidirectional(GRU(100, return_sequences=True, dropout=0.2, activation='elu'))(review_encoder)
            m3_l_lstm_sent = GRU(100, return_sequences=True, dropout=0.2, activation='elu')(m3_review_encoder)
            m3_l_att_sent = Attention()(m3_l_lstm_sent)
            m3_prepreds = Dense(50)(m3_l_att_sent)
            m3_out = Dense(labels_clinical_train.shape[1], activation='softmax', name='prediction_clinical')(m3_prepreds)

            # MODEL
            mtl_model = Model(review_input, [m1_out, m2_out, m3_out])
            # mtl_model.summary()

            mtl_model.compile(loss=self.multi_loss,
                              optimizer=AdamW(lr=0.001, beta_1=0.9, beta_2=0.999, weight_decay=1e-4, epsilon=1e-8, decay=0.),
                              metrics=['acc'])

            mtl_model.fit(
                X_train_cv,
                [labels_is_cancer_train_cv,
                 labels_cancer_type_train_cv,
                 labels_clinical_train_cv],
                validation_data=(X_valid_cv,
                                 [labels_is_cancer_valid_cv,
                                  labels_cancer_type_valid_cv,
                                  labels_clinical_valid_cv]),
                nb_epoch=epochs,
                batch_size=batch_size,
                callbacks=callback_list,
                verbose=0
            )

            results_test.append([
                callback_list[-1].final_evaluation[0],
                callback_list[-1].final_evaluation[1],
                callback_list[-1].final_evaluation[2]
            ])

            print('Values at last epoch on TEST:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}'.format(callback_list[-1].final_evaluation[0],
                                                                                                        callback_list[-1].final_evaluation[1],
                                                                                                        callback_list[-1].final_evaluation[2]))

            # DEV DATA SET
            predictions = mtl_model.predict(data_test)
            task_results=[]
            for index in range(0, len(predictions)):
                val_targ = np.argmax(validation_data[index], axis=1)
                val_predict = np.argmax(predictions[index], axis=1)
                _val_f1 = f1_score(val_targ, val_predict, average='weighted')
                _val_recall = recall_score(val_targ, val_predict, average='weighted')
                _val_precision = precision_score(val_targ, val_predict, average='weighted')
                task_results.append([_val_f1, _val_precision, _val_recall])
            results_dev.append(task_results)

            print('Predictions on DEV:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}'.format(task_results[0], task_results[1], task_results[2]))

            if self.save_best(_val_f1):
                mtl_model.save('{}/{}.h5'.format(self._storeDir, modelname))

        results_store = []

        results_store.append(self.data_loader.store_results([x[0] for x in results_test], 'is_cancer', 'MTL_HATT', dataset, 'test'))
        results_store.append(self.data_loader.store_results([x[1] for x in results_test], 'cancer_type', 'MTL_HATT', dataset, 'test'))
        results_store.append(self.data_loader.store_results([x[2] for x in results_test], 'clinical', 'MTL_HATT', dataset, 'test'))

        results_store.append(self.data_loader.store_results([x[0] for x in results_dev], 'is_cancer', 'MTL_HATT', dataset, 'dev'))
        results_store.append(self.data_loader.store_results([x[1] for x in results_dev], 'cancer_type', 'MTL_HATT', dataset, 'dev'))
        results_store.append(self.data_loader.store_results([x[2] for x in results_dev], 'clinical', 'MTL_HATT', dataset, 'dev'))

        df_store = pd.concat(results_store)
        df_store.to_csv(self._resultsDir+modelname+'.csv', sep='\t', index=False, header =df_store.columns)

        f1, p, r = np.mean(results_test, axis=0)
        f1_dev, p_dev, r_dev = np.mean(results_dev, axis=0)

        print("Average performance on {} TEST DATASET:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}".format(modelname, f1, p, r))
        print("Average performance on {} DEV DATASET:\n\tIS_CANCER: {} \n\tCANCER_TYPE: {} \n\tCLINICAL: {}".format(modelname, f1_dev, p_dev, r_dev))
        # input('All right mate?')

    def multi_loss(self, ys_true, ys_pred):
        # MEAN SQUARED ERROR FOR EACH OF THE TASK
        # assert len(ys_true) == self.nb_outputs and len(ys_pred) == self.nb_outputs
        loss = 0
        elems = (ys_true, ys_pred)
        alternate = K.map_fn(lambda x: (x[0] - x[1]) ** 2, elems, dtype=tf.float32)
        return K.sum(alternate, -1)

class VAE(object):
    
    def __init__(self):
        self.encoder = None
        self.decoder = None
        self.sentiment_predictor = None
        self.autoencoder = None
        self.vae = None
        self.parallel_model = None
        self.encoded = None

        self._adamw = AdamW(lr=0.001, beta_1=0.9, beta_2=0.999, weight_decay=1e-4, epsilon=1e-8, decay=0.)
        self._adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-5, decay=0.001)

        self._sgdw = SGDW(lr=0.01, momentum=0., decay=0., weight_decay=1e-4, nesterov=False)
        self._sgd = SGD(lr=0.01, decay=1e-6, momentum=1.9)
        
        self._epsilon_std = 0.01

        self._storeDir='./trained_models/'
        self._storeUtilities = './trained_models/utilityModels'
        self._resultsDir = 'results/'
        self._tBoardDir = './tboard'

        self._callback_list = [
            # EarlyStopping(
            #     monitor='val_loss',
            #     patience=3,
            #     min_delta=0.001
            # ),
            ModelCheckpoint(
                filepath='{}/{}.h5'.format(self._storeDir, 'VAE'),
                monitor='val_acc',
                save_best_only=True,
            ),
            CSVLogger(
                append=False,
                filename='{}/logs/{}.csv'.format(self._resultsDir, 'VAE')
             ),
            ReduceLROnPlateau(
                monitor='val_loss',
                factor=0.5,
                patience=2,
                min_lr=1e-5,
                verbose=0
            ),
            self.step_decay_schedule(
                initial_lr=1e-2,
                decay_factor=0.75,
                step_size=1
            ),
            TensorBoard(
                    log_dir=self._tBoardDir,
                    histogram_freq=0,
                    write_graph=True,
                    write_images=True
            )
        ]

    def vae_loss(self, x, x_decoded_mean, z_mean, z_log_var):
        x = K.flatten(x)
        x_decoded_mean = K.flatten(x_decoded_mean)
        # xent_loss = max_length * objectives.binary_crossentropy(x, x_decoded_mean)
        xent_loss = max_length * objectives.categorical_crossentropy(x, x_decoded_mean)
        kl_loss = - 0.5 * K.mean(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
        return xent_loss + kl_loss

    def sampling(self, args):
        z_mean_, z_log_var_ = args
        batch_size = K.shape(z_mean_)[0]
        epsilon = K.random_normal(shape=(batch_size, latent_rep_size), mean=0., stddev=self._epsilon_std)
        return z_mean_ + K.exp(z_log_var_ / 2) * epsilon

    def step_decay_schedule(self, initial_lr=1e-3, decay_factor=0.75, step_size=2):
    # Wrapper function to create a LearningRateScheduler with step decay schedule.

        def schedule(epoch):
            return initial_lr * (decay_factor ** np.floor(epoch / step_size))

        return LearningRateScheduler(schedule)

    def create(self, vocab_size=500, max_length=300, latent_rep_size=200, embedding_matrix=None):

        # rnInit = RandomNormal(mean=0.01, stddev=0.005, seed=None)
        x = Input(shape=(max_length,))
        x_embed = Embedding(embedding_matrix.shape[0],
                            embedding_matrix.shape[1],
                            weights=[embedding_matrix],
                            input_length=max_length,
                            trainable=False)(x)
        # else:
        #     x_embed = Embedding(vocab_size, 64, input_length=max_length, trainable=False, embeddings_initializer=rnInit)(x)

        vae_loss, self.encoded = self._build_encoder(x_embed, latent_rep_size=latent_rep_size, max_length=max_length)
        self.encoder = Model(inputs=x, outputs=self.encoded)

        encoded_input = Input(shape=(latent_rep_size,))
        # predicted_sentiment = self._build_class_predictor(encoded_input)
        # self.sentiment_predictor = Model(encoded_input, predicted_sentiment)
        #
        decoded = self._build_decoder(encoded_input, vocab_size, max_length)
        self.decoder = Model(encoded_input, decoded)

        # self.autoencoder = Model(inputs=x, outputs=[self._build_decoder(encoded, vocab_size, max_length), self._build_class_predictor(encoded)])

        # with tf.device("/cpu:0"):
            # initialize the model
        self.autoencoder=Model(x,self._build_decoder(self.encoded, vocab_size, max_length))
        self.autoencoder.summary()
        self.autoencoder.compile(
            # optimizer=adam,
            optimizer=self._adamw,
            loss=[vae_loss],#, 'binary_crossentropy'],
            metrics=['accuracy']
        )

    def _build_encoder(self, x, latent_rep_size=100, max_length=300, epsilon_std=0.01):

        def sampling(args):
            z_mean_, z_log_var_ = args
            batch_size = K.shape(z_mean_)[0]
            epsilon = K.random_normal(shape=(batch_size, latent_rep_size), mean=0., stddev=epsilon_std)
            return z_mean_ + K.exp(z_log_var_ / 2) * epsilon

        def vae_loss(x, x_decoded_mean):
            x = K.flatten(x)
            x_decoded_mean = K.flatten(x_decoded_mean)
            # xent_loss = max_length * objectives.binary_crossentropy(x, x_decoded_mean)
            xent_loss = max_length * objectives.categorical_crossentropy(x, x_decoded_mean)
            kl_loss = - 0.5 * K.mean(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
            return xent_loss + kl_loss

        h = GRU(500, return_sequences=True, name='lstm_1', activation='elu')(x)
        h = GRU(300, return_sequences=False, name='lstm_2', activation='elu')(h)
        h = Dense(250, activation='elu', name='dense_1')(h)
        h = Dropout(.2)(h)

        z_mean = Dense(latent_rep_size, name='z_mean', activation='elu')(h)
        z_log_var = Dense(latent_rep_size, name='z_log_var', activation='elu')(h)
        return (vae_loss, Lambda(sampling, output_shape=(latent_rep_size,), name='lambda')([z_mean, z_log_var]))

    def _build_decoder(self, encoded, vocab_size, max_length):
        repeated_context = RepeatVector(max_length)(encoded)

        h = GRU(300, return_sequences=True, name='dec_lstm_1', activation='elu')(repeated_context)
        h = GRU(500, return_sequences=True, name='dec_lstm_2', activation='elu')(h)
        decoded = TimeDistributed(Dense(vocab_size, activation='softmax'), name='decoded_mean')(h)
        # decoded = Dense(vocab_size, activation='softmax', name='decoded_mean')(h)
        return decoded

    def _build_class_predictor(self, encoded):
        h = Dense(100, activation='linear')(encoded)
        return Dense(1, activation='sigmoid', name='pred')(h)


# ALTERNATIVE IMPLEMENTATION
# https://github.com/mattiacampana/Autoencoders/blob/master/models/vae.py