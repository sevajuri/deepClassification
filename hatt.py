# author - Jurica Seva; modified from Richard Liao: https://github.com/richliao/textClassifier
# import comet_ml in the top of your file
import numpy as np
np.random.seed(42)

from comet_ml import Experiment
from utils.models import Models
from keras.preprocessing.text import Tokenizer
from utils.transformers import TokenizePreprocessor
from utils.data_loader import data_loader

import pandas as pd
from statistics import median
from sklearn.utils import shuffle

loader = data_loader()
tokenizer = TokenizePreprocessor()
keras_tokenizer = Tokenizer()

# Add the following code anywhere in your machine learning file
model = Models()

experiment = Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj",
                        project_name="hatt_cancer_type",
                        workspace="deakkon")
hatt_model = model.hatt('cancer_type', use_median=True,  experiment_id=experiment.get_key(), epochs=100)

experiment = Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj",
                        project_name="hatt_is_cancer",
                        workspace="deakkon")
hatt_model = model.hatt('is_cancer', use_median=True,  experiment_id=experiment.get_key(), epochs=100)

experiment = Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj",
                        project_name="hatt_clinical",
                        workspace="deakkon")
hatt_model = model.hatt('clinical', use_median=True,  experiment_id=experiment.get_key(), epochs=100)


# TRAIN: WHICH CANCER
# TRAIN: IS CLINICAL