import numpy as np
np.random.seed(42)

from utils.models import Models
from statistics import median
import morph

from utils.transformers import TokenizePreprocessor
from utils.data_loader import data_loader
from utils.models_sklearn import getBestModel
from keras.preprocessing.text import Tokenizer

from sklearn.model_selection import RandomizedSearchCV, train_test_split

model = Models()
dl = data_loader()
datasets = dl._trainData
input(datasets)
# datasets=[['civic', 'pubmed']]
tasks = dl._tasks
# print(tasks)
tasks_execute = [x for x in tasks if 'mtl' not in x]
# print(tasks)
results= []

for dataset in datasets:

    tokenizer = TokenizePreprocessor()
    keras_tokenizer = Tokenizer()

    # READ DATASETS
    dataset_name = "_".join(dataset)
    df = dl.read_corpora(dataset, dataset_name, oversample_by_copy=True)

    # TRAING CORPORA - RAW -> PREPARED
    corpora = df.text.values
    keras_tokenizer, word_to_index_dict, index_to_word_dict, MAX_SENTS, MAX_SENT_LENGTH, tokens = \
        dl.prepareForTraining(corpora, dataset_name, tokenizer, keras_tokenizer, median=True)

    # flat_corpus = [morph.flatten(tokens[i]) for i in range(0, len(tokens))]
    flat_corpus = [morph.flatten(x) for x in tokens]

    for task in tasks_execute:
        model_name = "{}_{}".format(task, dataset_name)
        labels = dl.get_labels(df.loc[:, tasks[task]['label_name']], model_name)
        labels = labels.argmax(1)

        x_train, x_val, y_train, y_val = train_test_split(flat_corpus,
                                                          labels,
                                                          test_size=0.15,
                                                          stratify=labels)
        gbm = getBestModel(model_name, x_train, y_train, dataset_name, task, testData=x_val, testLabels=y_val)

        try:
            gbm.getBestModel()
        except Exception as e:
            print(e)
            print("ERRORS HAVE HAPPENED O LORD O LORD", dataset_name, task)
            input('errors all around')
