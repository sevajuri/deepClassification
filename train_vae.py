import numpy as np
np.random.seed(42)

import morph
from keras.preprocessing.text import Tokenizer
from utils.transformers import TokenizePreprocessor
from sklearn.model_selection import train_test_split
from utils.models import VAE
from utils.data_loader import data_loader
from statistics import median
from utils.utils import KerasBatchGenerator_VAE
from nltk.corpus import stopwords
import pubmed_parser as pp


from os import listdir
from os.path import isfile, join

pubmed_dir = "/mnt/fob-wbia-vol2/wbi/sevajuri/pubmed/"
pubmed_data = [f for f in listdir(pubmed_dir) if isfile(join(pubmed_dir, f))]

def text_generator(pubmed_data):
    fields = ["title", "text"]
    for gz_file in pubmed_data:

        dicts_out = pp.parse_medline_xml(pubmed_dir+gz_file,
                                         year_info_only=False,
                                         nlm_category=False)

        for document in dicts_out:
            yield(" ".join([document[item] for item in fields if item in document]))

dl = data_loader()
dataset = ['cgi_biomarkers', 'civic', 'pubmed']
task = 'is_cancer'
tokenizer = TokenizePreprocessor()
keras_tokenizer = Tokenizer()

# READ DATASETS
dataset_name = "_".join(dataset)
df = dl.read_corpora(dataset, dataset_name, oversample_by_copy=True)
corpora = df.text.values
keras_tokenizer, word_to_index_dict, index_to_word_dict, MAX_SENTS, MAX_SENT_LENGTH, tokens = \
    dl.prepareForTraining(corpora, dataset_name, tokenizer, keras_tokenizer, median=True)

tokens = [morph.flatten(x) for x in tokens]

MAX_LENGTH = int(median([len(x) for x in tokens]))
NUM_WORDS = len(keras_tokenizer.word_index)+1

data = np.zeros((len(tokens), MAX_LENGTH), dtype='int32')
for i, documents in enumerate(tokens):
    for k, token in enumerate(documents):
        if k < MAX_LENGTH:
            data[i, k] = keras_tokenizer.word_index[token]

model_name = "{}_{}".format(task, dataset_name)
embedding_matrix, _ = dl.get_embedding_layer(keras_tokenizer.word_index, MAX_LENGTH, model_name)

# "Splitting dataset in train test (0.7/0.3)"
X_train, X_test = train_test_split(data, test_size=0.15, random_state=42)
print(X_train.shape, X_test.shape, NUM_WORDS)

# temp = np.zeros((X_train.shape[0], MAX_LENGTH, NUM_WORDS))
# temp[np.expand_dims(np.arange(X_train.shape[0]), axis=0).reshape(X_train.shape[0], 1),
#      np.repeat(np.array([np.arange(MAX_LENGTH)]), X_train.shape[0], axis=0), X_train] = 1
# X_train_one_hot = temp
#
# temp = np.zeros((X_test.shape[0], MAX_LENGTH, NUM_WORDS))
# temp[np.expand_dims(np.arange(X_test.shape[0]), axis=0).reshape(X_test.shape[0], 1),
#      np.repeat(np.array([np.arange(MAX_LENGTH)]), X_test.shape[0], axis=0), X_test] = 1
# x_test_one_hot = temp
# print(X_train_one_hot.shape,x_test_one_hot.shape)

batch_size = 15
training_generator = KerasBatchGenerator_VAE(batch_size, X_train, MAX_LENGTH, NUM_WORDS)
validation_generator = KerasBatchGenerator_VAE(batch_size, X_test, MAX_LENGTH, NUM_WORDS)

# VAE TRAIN
vae = VAE()
vae.create(NUM_WORDS, MAX_LENGTH, embedding_matrix=embedding_matrix)
vae.autoencoder.fit_generator(
    generator=training_generator,
    validation_data=validation_generator,
    epochs=150,
    callbacks=vae._callback_list
)
